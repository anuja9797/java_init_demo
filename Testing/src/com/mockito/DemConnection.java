package com.mockito;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DemConnection {

	Connection conn;
	PreparedStatement stmt;
	public DemConnection() {
		// TODO Auto-generated constructor stub
	}
	public Connection getConnection()
	{
		conn=null;
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			System.out.println("DRIVER LOADED");	
			conn=DriverManager.getConnection("jdbc:oracle:thin:@vhkdld387:1551:staffd","hr","Eagle#2019"); //url, username, pwd
			System.out.println("Connected");
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return conn;
	}
	
	public DemConnection(Connection conn, PreparedStatement stmt) {
		super();
		this.conn = conn;
		this.stmt = stmt;
	}
	public int getRecord(String query) throws SQLException
	{
		return conn.createStatement().executeUpdate(query);
	}
}
