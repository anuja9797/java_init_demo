package com.myBeans;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class myBean implements ApplicationContextAware {

	private ApplicationContext context;
	public myBean() {
		// TODO Auto-generated constructor stub
		System.out.println("-----------------------------------");
		System.out.println("myBean DEFAULT CONSTRUCTOR");
	}
	@Override
	public void setApplicationContext(ApplicationContext ctx) throws BeansException {
		// TODO Auto-generated method stub
		System.out.println("-----------------------------------");
		System.out.println("CONTEXT SET");
		this.context=ctx;
	}
	public void Display()
	{
		//ApplicationContext context=new ClassPathXmlApplicationContext("beans.xml");
		System.out.println("-----------------------------------");
		DemoCustomInit demo=(DemoCustomInit)context.getBean("d");
		System.out.println(demo);
	}
}
