import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.table_per_hierarchy.Employee;
import com.table_per_hierarchy.WageEmployee;



public class Test_Inheritance {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Configuration configuration = new Configuration().configure();
	  
		
		
			try(
				SessionFactory sessionFactory = configuration.buildSessionFactory();
				Session session = sessionFactory.openSession();)
			{
			
			session.beginTransaction();
			Employee emp=new Employee("Nikita","Pune");
			WageEmployee wemp=new WageEmployee(121,141,"Shreya","Nashik");
			session.save(emp);
			session.save(wemp);
			session.getTransaction().commit();
			
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
	}

}
/*
 
   create table Anu_EmployeeConcrete (
       empId number(10,0) not null,
        address varchar2(255 char),
        name varchar2(255 char),
        primary key (empId)
    )
    create table Anu_WageEmployeeConcrete (
       empId number(10,0) not null,
        address varchar2(255 char),
        name varchar2(255 char),
        hours number(10,0) not null,
        wages number(10,0) not null,
        primary key (empId)
    )

       
     insert 
    into
        Anu_Employee
        (address, name, type, empId) 
    values
        (?, ?, 'employee', ?)

    insert 
    into
        Anu_Employee
        (address, name, hours, wages, type, empId) 
    values
        (?, ?, ?, ?, 'wageemployee', ?) 
 */
 