package com.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

@Configuration // imp to write
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private AuthenticationEntryPoint authEntryPoint;

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		// All url in this function are tested whether they are safe or not(secured/not)
		// http.csrf().disable().authorizeRequests().anyRequest().authenticated().and().httpBasic().authenticationEntryPoint(authEntryPoint);

		// http.antMatcher("/message").authorizeRequests().anyRequest().hasRole("ADMIN").anyRequest().authenticated().and().httpBasic();

		http.authorizeRequests().antMatchers("/message").access("hasRole('ROLE_USER')").anyRequest().authenticated()
				.and().httpBasic();
	}

	

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {

		// {noop}-passwordEncoder
		System.out.println("configure");
		auth.inMemoryAuthentication().withUser("user1")
				.password("$2a$10$mOcb8sj9qEbBd2RhUOUO5uKSY9.w0UR9x/f45Elc/3Ae3I/TVlQYy").roles("USER");// Only user1
																										// with
																										// password="password"
																										// allowed to
																										// login
		auth.inMemoryAuthentication().withUser("admin1")
				.password("{noop}password").roles("ADMIN", "MANAGAER");// Only
																														// user1
																														// with
																														// password="password"
																														// allowed
																														// to
																														// login

		// Bcypt of pasword =
		// $2a$10$mOcb8sj9qEbBd2RhUOUO5uKSY9.w0UR9x/f45Elc/3Ae3I/TVlQYy

	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		System.out.println("hello");
		return new BCryptPasswordEncoder();

	}
}
