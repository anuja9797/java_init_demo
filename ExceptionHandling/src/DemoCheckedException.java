import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class DemoCheckedException {
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		// reader = null;
		
		try (
				FileReader reader=new FileReader("C:/Users/watpadea/eclipse-workspace/ExceptionHandling/src/emp.txt");
				//FileWriter writer=new FileWriter("C:/Users/watpadea/eclipse-workspace/ExceptionHandling/src/emp.txt");
				)
		{
			byte b;
			do
			{
				b=(byte) reader.read();
				if(b != -1)
				System.out.print((char)b);
			    
			}while(b != -1);
			
			reader.close();
		} 
		catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		finally {
//			reader.close();
//		}
	
	}
}
