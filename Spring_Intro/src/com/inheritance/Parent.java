package com.inheritance;

public class Parent {

	private int id;
	private String data;
	@Override
	public String toString() {
		return "Parent [id=" + id + ", data=" + data + "]";
	}
	public Parent() {
		// TODO Auto-generated constructor stub
	}
	
	public Parent(int id, String data) {
		super();
		this.id = id;
		this.data = data;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	
}
