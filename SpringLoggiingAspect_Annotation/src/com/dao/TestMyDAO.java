package com.dao;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestMyDAO {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ApplicationContext context=new ClassPathXmlApplicationContext("connection_new.xml");
		MyDAO dao=(MyDAO)context.getBean("myDAO");
		dao.add();
		dao.data(-1, 2);
		System.out.println("--VALUE: "+dao.myMethod());
	}

}
