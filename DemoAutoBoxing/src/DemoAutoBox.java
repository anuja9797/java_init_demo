import java.io.File;

public class DemoAutoBox {

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Object o=new File("myfile.txt");
		
		((File)o).canRead();
		//OR
		File file=(File)o;
		file.canRead();
		
		
		//Object o1=new Thread();
		
		Integer i=new Integer("10");
		//OR
		Integer i1=new Integer(10);
		
		i.intValue();
		i.byteValue();
		i.doubleValue();
		//easy conversion
		
		i=100;
		//automatic obj creation: i=100 => i=new Integer("100");   <==AUTOBOXING

		//autounboxing
		
		int x=i;  //x=i.intValue()
	
		double y=i;  //y=i.doubleValue();
		
		Object obj=i; // INVALID OPERATION! =>always use INTEGER and not OBJECT
		int z=((Integer)obj).intValue();
		System.out.println(z);
	}

}
