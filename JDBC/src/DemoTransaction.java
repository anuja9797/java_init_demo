import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

public class DemoTransaction {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			System.out.println("DRIVER LOADED");	
			Connection conn=DriverManager.getConnection("jdbc:oracle:thin:@vhkdld387:1551:staffd","hr","Eagle#2019"); //url, username, pwd
			System.out.println("Connected");
			
			conn.setAutoCommit(false);         //Sets autocommit false
			
			Scanner inp=new Scanner(System.in);
			System.out.println("Enter isbn:");
			int isbn=inp.nextInt();
			System.out.println("Enter Name:");
			String name=inp.next();
			System.out.print("Enter Author:");
			String author=inp.next();
			System.out.println("Enter Price:");
			int price=inp.nextInt();
			String INSERT_TABLE=" insert into anu_book values(?,?,?,?)";
		
			PreparedStatement ps=conn.prepareStatement(INSERT_TABLE);
			ps.setInt(1,isbn);
			ps.setString(2,name);
			ps.setString(3,author);
			ps.setInt(4,price);
			int ret=ps.executeUpdate();
			if(ret!=0)
				System.out.println("Added "+ret+" rows");
			inp.close();
			
			conn.commit();               //manual commit
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			
			if(e.getErrorCode()==00001)
				System.out.println("Primary Key");
			e.printStackTrace();
		}
	
	}

}
