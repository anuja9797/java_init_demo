package com.validators;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.pojo.Book;



public class BookValidator implements Validator{

	@Override
	public boolean supports(Class<?> book_class) {
		// TODO Auto-generated method stub
		return book_class.equals(Book.class);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		// TODO Auto-generated method stub
		Book book=(Book)obj;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors,"bookName","bookName.required");
		//"bookName.required" is USERDEFINED KEY
		ValidationUtils.rejectIfEmptyOrWhitespace(errors,"author","author.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors,"description","description.required");
		
		if(book.getDescription().length()<5 ||book.getDescription().length()>10)
		{
			errors.rejectValue("description", "description.length", "BETWEEN 5 TO 10 CHARACTER");
		}
		if(book.getISBN()<=1500)
		{
			errors.rejectValue("ISBN", "ISBN.required", "INCORRECT ISBN");
		}
		ValidationUtils.rejectIfEmptyOrWhitespace(errors,"price","price.incorrect");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors,"publication","publication.required");
		
	}

	
}
