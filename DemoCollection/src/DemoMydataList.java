import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import com.beans.MyData;

public class DemoMydataList {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		List<MyData> list=new ArrayList<>();
		
		list.add(new MyData(1,"abc",1201));
		list.add(new MyData(2,"xyz",1201));
		list.add(new MyData(3,"pqr",1202));
		list.add(new MyData(4,"apr",1203));
		list.add(new MyData(1,"abc",1201));
		
		//System.out.println(list);
		
		Iterator<MyData> itr=list.iterator();
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
		System.out.println("");
		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			MyData myData = (MyData) iterator.next();
			System.out.println(myData);
		}
		
		
		MyData obj=list.remove(3);//remove by index
		System.out.println("\n\nREMOVED "+obj);
		
		
		MyData data=new MyData(1,"abc",1201);
		//list.removeAll(Collections.singleton(data));    //removes All
		//list.add(data); if you add by reference and delete by reference..all objects of that ref  will be deleted
//		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
//			MyData myData = (MyData) iterator.next();
//			System.out.println(myData);
//		}
		
		boolean b=list.remove(data);  //remove by value==> equals method NEEDED
		System.out.println("\n\nREMOVED "+data+" "+b);
		
		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			MyData myData = (MyData) iterator.next();
			System.out.println(myData);
		}
		
		System.out.println("\n CONTAINS  :"+list.contains(new MyData(3,"pqr",1202)));
	}

}
