package com.singleton;

public class Person_StaticBlock {

	private String name;
	@Override
	public String toString() {
		return "Person_StaticBlock [name=" + name + "]";
	}

	private static Person_StaticBlock person_static;
	private Person_StaticBlock() {
		// TODO Auto-generated constructor stub
		name="STATIC BLOCK";
	}
	static{
		person_static=new Person_StaticBlock();
	}
	public static Person_StaticBlock getInstance()
	{
		return person_static;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Person_StaticBlock p=Person_StaticBlock.getInstance();
		//System.out.println(p);
		String name=p.toString();
		System.out.println(name);
	}
	

}
