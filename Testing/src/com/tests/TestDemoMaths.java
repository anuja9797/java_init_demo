package com.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.operation.DemoMaths;

public class TestDemoMaths {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAdd_POSITIVE() { 
		DemoMaths obj=new DemoMaths();
		int arg1=200;
		int arg2=100;
		int res=obj.add(arg1, arg2);
//		if(res==300)
//			System.out.println("SUCCESS");
//		else
//			System.out.println("FAILURE");                    //if-else not trustworthy
		
		assertEquals(300,res);
		
	}
	@Test
	public void testAdd_NEGATIVE() { 
		DemoMaths obj=new DemoMaths();
		int arg1=100;
		int arg2=-100;
		int res=obj.add(arg1, arg2);		
		assertEquals(0,res);
		
	}
	@Test
	public void testDiv_POSITIVE() {
		DemoMaths obj=new DemoMaths();
		int arg1=100;
		int arg2=2;
		int res=obj.div(arg1, arg2);		
		assertEquals(50,res);
	}
	@Test
	public void testDiv_NEGATIVE() {
		DemoMaths obj=new DemoMaths();
		int arg1=11;
		int arg2=2;
		int res=obj.div(arg1, arg2);
//		assertEquals(1,obj.div(12, 12));
//		assertEquals(1,obj.div(12, 12));
//		assertEquals(5.5,res);
//		assertEquals(1,obj.div(12, 12));  //whole test block is failed
		
		assertEquals(1,obj.div(12, 12));
		assertEquals(1,obj.div(12, 12));
		assertNotEquals(5.5,res);
		assertEquals(1,obj.div(12, 12));
		
	}
	
	@Test
	public void testSub() {

		DemoMaths obj=new DemoMaths();
		int arg1=100;
		int arg2=20;
		int res=obj.sub(arg1, arg2);		
		assertEquals(80,res);
	}

	@Test
	public void testMult_POSITIVE() {

		DemoMaths obj=new DemoMaths();
		int arg1=10;
		int arg2=20;
		int res=obj.mult(arg1, arg2);		
		assertEquals(200,res);
		
	}
	@Test
	public void testMult_NEGATIVE() {

		DemoMaths obj=new DemoMaths();
		int arg1=-10;
		int arg2=-10;
		int res=obj.mult(arg1, arg2);		
		assertEquals(100,res);
		
	}

}
