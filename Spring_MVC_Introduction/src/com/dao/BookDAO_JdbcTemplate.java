package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.pojo.Book;

@Repository(value="BookDAO_JdbcTemplate")
public class BookDAO_JdbcTemplate implements BookDAO {

	@Autowired
	JdbcTemplate jdbctemplate;
	@Override
	public int addBook(Book book) {
		// TODO Auto-generated method stub
		
		int rows=0;
		String insert="insert into anu_books values(?,?,?,?,?,?)";
		rows=jdbctemplate.update(insert,book.getBookName(),book.getISBN(),book.getPublication(),book.getDescription(),book.getAuthor(),book.getPrice());
		return rows;
	}

	@Override
	public int updateBook(long ISBN, int price) {
		// TODO Auto-generated method stub
		int rows=0;
		String update="update anu_books set price=? where ISBN=?";
		rows=jdbctemplate.update(update,price,ISBN);
		return rows;
	}

	@Override
	public boolean deleteBook(long ISBN) {
		// TODO Auto-generated method stub
		int rows=0;
		//Object[] param= {ISBN};
		String delete="delete from anu_books where ISBN=?";
		jdbctemplate.update(delete,ISBN);
		return true;
	}

	@Override
	public List<Book> findAllBooks() {
		// TODO Auto-generated method stub
		BookMapper bookmapper=new BookMapper();
		int rows=0;
		String sql="select * from anu_books";
		List<Book> list=jdbctemplate.query(sql, bookmapper);
		return list;
	}

}
