package com.facade;

import java.io.File;

public class MyFacade {

	public File convertVideo(String filename, String format)
	{
		System.out.println("VideoConversion started");
		VideoFile file=new VideoFile(filename);
		Codec sourceCodec =CodecFactory.extract(file);
		Codec destinationCodec;
		if(format.equals("mp4"))
		{
			destinationCodec=new CompressionCodec();
		}
		else
		{
			destinationCodec=new MPEG4CompressionCodec();
		}
		VideoFile videofile=new VideoFile("myfile");
		File result=(new AudioMixer()).fix(videofile);
		System.out.println("VideoConversion completed");
		return result;
				
	}
}
