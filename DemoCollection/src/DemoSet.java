import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class DemoSet {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

			Set<Integer> intlist=new TreeSet();
			//Set<Integer> intlist=new HashSet<>();
			intlist.add(100);intlist.add(200);intlist.add(300);intlist.add(400);intlist.add(500);
			intlist.add(100);//avoids duplicates
			
			
			//intlist.add(2,9); no indexing
			System.out.println(intlist.size());
			System.out.println(intlist);
			
			Integer obj=new Integer(99);//BOXING-UPCASTING
			intlist.add(obj);			 

			Iterator<Integer> itr=intlist.iterator();
		
			while(itr.hasNext())
			{	Integer i=itr.next();
				//Integer i=itr.next();//Downcasting not needed
				//System.out.println(i);
				if(i>100) 
				{
					System.out.println(i);
				}
			}
			for(int value:intlist)
			{
				System.out.println(value);
			}
			//intlist.get(0); no indexing
//			intlist.remove(0);//using index
//			intlist.remove(new Integer("200")); //using value=>Cast it into Object
			System.out.println(intlist);
	}

}
