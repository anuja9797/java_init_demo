<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<c:set var="animal_name" value="${param.animal}"> </c:set>
	<c:if test="${animal_name == 'TIGER' }">IT IS A TIGER</c:if>
	<c:if test="${animal_name eq 'LION' }">IT IS A LION</c:if>
	
	<br>
	<c:choose>
		<c:when test="${animal_name == 'TIGER' }">TIGER IT IS
		</c:when>
		<c:when test="${animal_name == 'LION' }">LION IT IS
		</c:when>
		<c:when test="${animal_name == 'ELEPHANT'}">ELEPHANT IT IS
		</c:when>
		<c:otherwise>INVALID CHOICE
		</c:otherwise>
	</c:choose>
</body>
</html>