import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.beans.association.Address_One_to_Many;
import com.beans.association.Person_One_to_Many;
import com.beans.association.joinColumn.Address_OnrToMany_Col;
import com.beans.association.joinColumn.Person_OneToMany_Col;

public class Test_Onetomany_Col {
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Configuration configuration = new Configuration().configure();
	    List<Address_OnrToMany_Col> addresses=new ArrayList<Address_OnrToMany_Col>();
	    Person_OneToMany_Col p=new Person_OneToMany_Col();
		
		
			try(
				SessionFactory sessionFactory = configuration.buildSessionFactory();
				Session session = sessionFactory.openSession();)
			{
			
			session.beginTransaction();
			addresses.add(new Address_OnrToMany_Col(401,"Nashik",456006l));
			addresses.add(new Address_OnrToMany_Col(502,"Mumbai",96356l));
			addresses.add(new Address_OnrToMany_Col(603,"Jalgaon",91005l));
			p.setAddress(addresses);
			p.setName("Nikita");
			//JOIN COL person_id in address table
			session.save(p);
			
			
			
			session.getTransaction().commit();
			
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		
		
	}
}
/*
Hibernate: create sequence hibernate_sequence start with 1 increment by  1
Hibernate: 
    
    create table Anu_AddOtoM (
       id number(10,0) not null,
        city varchar2(255 char),
        pincode number(19,0),
        primary key (id)
    )
Hibernate: 
    
    create table Anu_AddOtoMCol (
       id number(10,0) not null,
        city varchar2(255 char),
        pincode number(19,0),
        person_id number(10,0),
        primary key (id)
    )
Hibernate: 
    
    create table Anu_PerOtoM (
       personId number(10,0) not null,
        name varchar2(255 char),
        primary key (personId)
    )
Hibernate: 
    
    create table Anu_PerOtoM_Anu_AddOtoM (
       Person_One_to_Many_personId number(10,0) not null,
        addresses_id number(10,0) not null
    )
Hibernate: 
    
    create table Anu_PerOtoM_Col (
       personId number(10,0) not null,
        name varchar2(255 char),
        primary key (personId)
    )
Hibernate: 
    
    alter table Anu_PerOtoM_Anu_AddOtoM 
       add constraint UK_1to26u73otm7av97x97nwbwyf unique (addresses_id)
Hibernate: 
    
    alter table Anu_AddOtoMCol 
       add constraint FKe8cf8c3l9pv5ohfxms3n2xouk 
       foreign key (person_id) 
       references Anu_PerOtoM_Col
Hibernate: 
    
    alter table Anu_PerOtoM_Anu_AddOtoM 
       add constraint FK91jvyhnn182jimqw8a1f6f5pn 
       foreign key (addresses_id) 
       references Anu_AddOtoM
Hibernate: 
    
    alter table Anu_PerOtoM_Anu_AddOtoM 
       add constraint FK4rdx9fgkfcx8l2ksfniyaea1m 
       foreign key (Person_One_to_Many_personId) 
       references Anu_PerOtoM
Aug 07, 2019 11:00:24 AM org.hibernate.tool.schema.internal.SchemaCreatorImpl applyImportSources
INFO: HHH000476: Executing import script 'org.hibernate.tool.schema.internal.exec.ScriptSourceInputNonExistentImpl@1b1637e1'
Hibernate: 
    select
        hibernate_sequence.nextval 
    from
        dual
Hibernate: 
    select
        address_on_.id,
        address_on_.city as city2_1_,
        address_on_.pincode as pincode3_1_ 
    from
        Anu_AddOtoMCol address_on_ 
    where
        address_on_.id=?
Hibernate: 
    select
        address_on_.id,
        address_on_.city as city2_1_,
        address_on_.pincode as pincode3_1_ 
    from
        Anu_AddOtoMCol address_on_ 
    where
        address_on_.id=?
Hibernate: 
    select
        address_on_.id,
        address_on_.city as city2_1_,
        address_on_.pincode as pincode3_1_ 
    from
        Anu_AddOtoMCol address_on_ 
    where
        address_on_.id=?
Hibernate: 
    insert 
    into
        Anu_PerOtoM_Col
        (name, personId) 
    values
        (?, ?)
Hibernate: 
    insert 
    into
        Anu_AddOtoMCol
        (city, pincode, id) 
    values
        (?, ?, ?)
Hibernate: 
    insert 
    into
        Anu_AddOtoMCol
        (city, pincode, id) 
    values
        (?, ?, ?)
Hibernate: 
    insert 
    into
        Anu_AddOtoMCol
        (city, pincode, id) 
    values
        (?, ?, ?)
Hibernate: 
    update
        Anu_AddOtoMCol 
    set
        person_id=? 
    where
        id=?
Hibernate: 
    update
        Anu_AddOtoMCol 
    set
        person_id=? 
    where
        id=?
Hibernate: 
    update
        Anu_AddOtoMCol 
    set
        person_id=? 
    where
        id=?
*/