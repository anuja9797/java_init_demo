package com.function;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import com.pojo.Student;

public class DemoFunction_apply {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		List<Student> students=Arrays.asList(
				new Student(1,"anuja",80.45d),
				new Student(2,"nikita",81.44d),
				new Student(3,"juilee",82.43d),
				new Student(4,"apurva",83.42d)
				);
		
		Function<Student, Double> fun=(Student s)->{
			return s.getPercentage();
		};
		Function<Student, Integer> totalfun=(Student s)->{
			int total=0;
			
			for(int m:s.getMarks())
			{total=total+m;}			
			return total;
		};
		
		Function<Student, Double> avgfun=(Student s)->{
			return s.getTotal()/3.0;
		};
		
		
		List<Double> result=convertStudentlistToPerList(students, fun);
		System.out.println(result);
		
		
		List<Student> students1=Arrays.asList(
				new Student(1,"anuja",new int[] {80,90,85}),
				new Student(2,"nikita",new int[] {80,95,91}),
				new Student(3,"juilee",new int[] {82,92,85}),
				new Student(4,"apurva",new int[] {90,90,95})
				);
		
		students1.forEach((s)->{s.setTotal(totalfun.apply(s));});
		students1.forEach((s)->{s.setPercentage(avgfun.apply(s));});
		
		students1.forEach((s)->{System.out.println(s);});
//		for(Student s:students1)
//			System.out.println(s);
		
		//Parameter,Parameter,Return
		BiFunction<Integer,Integer,Integer> add=(Integer a,Integer b)->{ return a+b;};
		int c=add.apply(23,22);
		System.out.println(c);
		
		BiFunction<String,String,String> addstr=(String a,String b)->{ return a+b;};
		String cstr=addstr.apply("Anuja"," Watpade");
		System.out.println(cstr);
		
		BiFunction<Integer,Integer,Void> displayadd=(Integer a,Integer b)->{ 
			System.out.println(a+b);
			return null;
			};
		System.out.println(displayadd.apply(3,2));
	}

	public static List<Double> convertStudentlistToPerList(List<Student> slist,Function<Student,Double> fun)
	{
		List<Double> plist =new ArrayList<>();
		for(Student s:slist)
		{
			plist.add(fun.apply(s));
		}
		return plist;
	}
	
}
