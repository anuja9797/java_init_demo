import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.HashMap;
import java.util.Iterator;
public class DemoMap {

	@SuppressWarnings("unlikely-arg-type")
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Map<String, Integer> map=new HashMap<String,Integer>();
		
		map.put("A", 100);
		map.put("N", 200);
		map.put("C", 300);
		map.put("D", 900);
		map.put("C", 800); //gets overrided
		
		System.out.println(map);
		
		System.out.println(map.get(100));
		
		System.out.println(map.get("N"));
		System.out.println(map.get("n"));
		
		Set<Entry<String, Integer>> entryset=map.entrySet();
		System.out.println(entryset);
		Iterator<Entry<String, Integer>> itr=entryset.iterator();
		while(itr.hasNext())
		{
			Entry<String, Integer> e=itr.next();
			System.out.println("KEY: "+e.getKey()+" VALUE: "+e.getValue());
		}
		
		for(Entry<String, Integer> e:entryset)
			System.out.println("KEY: "+e.getKey()+" VALUE: "+e.getValue());
	}

}
