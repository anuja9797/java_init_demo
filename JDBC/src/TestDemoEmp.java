import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import com.pojo.Employee;

public class TestDemoEmp {
	public static Employee addEmp()
	{    Scanner inp=new Scanner(System.in);
		System.out.println("Enter Id:");
		int id=inp.nextInt();
		System.out.println("Enter Name:");
		String name=inp.next();
		System.out.println("Enter Address:");
		String addr=inp.next();
		System.out.println("Enter age:");
		int age=inp.nextInt();
		System.out.println("Enter Experience:");
		int exp=inp.nextInt();
		System.out.println("Enter dept:");
		String dept=inp.next();
		inp.close();
		return new Employee(id,name,addr,age,exp,dept);
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			DemoEmployee d=new DemoEmployee();
			int choice;
			Scanner inp=new Scanner(System.in);
			
			do {
			System.out.println("\n\n1:addEmp 2:Update 3:GetByID 4:getAll 5:getAllByEXP 6:getAllbyDept 7:exit\nEnter:");
			choice=inp.nextInt();
			switch(choice)
			{
			case 1:
				Employee e=addEmp();
				int ret=d.addEmployee(e);
				if(ret!=0)
				{System.out.println("Added "+ret+" rows");}
				else
					System.out.println("Not added");
				break;
			case 2:
				Employee e1=addEmp();
				if(d.updateEmployee(e1.getEmpId(),e1))
				{
					System.out.println("UPDATED");
				}else
					System.out.println("OOPS");
				break;
			case 3:
				System.out.println("Enter ID:");
				int id1=inp.nextInt();
				Employee e3=d.getEmployeeById(id1);
				if(e3!=null)
					System.out.println(e3);
				else
					System.out.println("NOT FOUND");
				break;
			case 4:
				List<Employee> list=d.getAllEmployee();
				Iterator<Employee> itr=list.iterator();
				while(itr.hasNext())
				{
					System.out.println(itr.next());
				}
				break;
			case 5:
				System.out.println("Enter Experience:");
				int exp1=inp.nextInt();
				List<Employee> list1=d.getAllEmployeeByExp(exp1);
				itr=list1.iterator();
				while(itr.hasNext())
				{
					System.out.println(itr.next());
				}
				break;
			case 6:
				System.out.println("Enter DEPT:");
				String dept=inp.next();
				List<Employee> list2=d.getAllEmployeeByDept(dept);
				itr=list2.iterator();
				while(itr.hasNext())
				{
					System.out.println(itr.next());
				}
				break;
			}
			}while(choice!=7);
			inp.close();
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
