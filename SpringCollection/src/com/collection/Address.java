package com.collection;

public class Address implements Comparable{
private String city;
private String pincode;

public Address() {
	// TODO Auto-generated constructor stub
}
public Address(String city, String pincode) {
	super();
	this.city = city;
	this.pincode = pincode;
}
@Override
public String toString() {
	return "Address [city=" + city + ", pincode=" + pincode + "]";
}
public String getCity() {
	return city;
}
public void setCity(String city) {
	this.city = city;
}
public String getPincode() {
	return pincode;
}
public void setPincode(String pincode) {
	this.pincode = pincode;
}
@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((city == null) ? 0 : city.hashCode());
	result = prime * result + ((pincode == null) ? 0 : pincode.hashCode());
	return result;
}
@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Address other = (Address) obj;
	if (city == null) {
		if (other.city != null)
			return false;
	} else if (!city.equals(other.city))
		return false;
	if (pincode == null) {
		if (other.pincode != null)
			return false;
	} else if (!pincode.equals(other.pincode))
		return false;
	return true;
}
@Override
public int compareTo(Object arg0) {
	// TODO Auto-generated method stub
	Address add=(Address)arg0;
	 if(this.getCity().equals(add.getCity()))
		 return 0;
	 else
		 return 1;
}


}
