package com.pojo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component(value="myadd")
public class Address {
@Value(value="pune")
private String city;
@Value(value="422115")
private String pincode;

public Address() {
	city="default";
	pincode="default";
	// TODO Auto-generated constructor stub
}
public Address(String city, String pincode) {
	super();
	this.city = city;
	this.pincode = pincode;
}
@Override
public String toString() {
	return "Address [city=" + city + ", pincode=" + pincode + "]";
}
public String getCity() {
	return city;
}
public void setCity(String city) {
	this.city = city;
}
public String getPincode() {
	return pincode;
}
public void setPincode(String pincode) {
	this.pincode = pincode;
}
}
