//import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class DemoListModify {
public static void main(String[] args) {
	//List<String> list=new ArrayList<String>();
	List<String> list=new CopyOnWriteArrayList<String>();
	list.add("a");
	list.add("b");
	list.add("c");
	list.add("d");
	list.add("e");
	Iterator<String> itr=list.iterator();
	while(itr.hasNext())
	{
		String s=itr.next();
		System.out.println("listval: "+s);
		if(s.equals("b"))
		{
			list.remove("b");
			list.add("v");
			list.add("z");
		}
	}
	System.out.println();
	itr=list.iterator();
	while(itr.hasNext())
	{
		
		System.out.println("listval: "+itr.next());
		
	}
}
}
