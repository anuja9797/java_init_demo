package com.aspects;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect
public class MyLoggingAspect {
	Logger logger=Logger.getLogger(getClass());

//	@Before(value="execution(* com.dao.MyDAO.myMethod(..))")
	@After(value="execution(* com.dao.MyDAO.*(..))")
	public void beforeAdvise(JoinPoint joinpoint)
	{
		logger.info("method will be invoked:-"+joinpoint.getSignature());
	}
	
	@Around(value="execution(* com.dao.MyDAO.myMethod(..))")
	public int aroundAdvise(ProceedingJoinPoint jp)
	{
		long start=System.currentTimeMillis();
		logger.info("AROUND ADVISE BEFORE "+jp.getSignature()+" B.L. METHOD GETTING INVOKED");
		Integer o=null;
		try {
			o=(Integer)jp.proceed();
			logger.info("NUMBER OF ROWS AFFECTED "+o);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		logger.info("AROUND ADVISE AFTER "+jp.getSignature()+" B.L. METHOD GETTING INVOKED");
		long end=System.currentTimeMillis();
		logger.info(jp.getSignature()+" TOOK  "+(end-start)+" time");
		logger.info(o.intValue());
		return o.intValue();
	}
}


























/*
//BEFORE
675   [main] INFO  com.aspects.MyLoggingAspect  - method will be invoked:-void com.dao.MyDAO.add()
--added
690   [main] INFO  com.aspects.MyLoggingAspect  - method will be invoked:-void com.dao.MyDAO.data(int,int)
--DATA: 1
690   [main] INFO  com.aspects.MyLoggingAspect  - method will be invoked:-int com.dao.MyDAO.myMethod()
--mymethod invoked

//AFTER
--added
647   [main] DEBUG org.springframework.beans.factory.support.DefaultListableBeanFactory  - Returning cached instance of singleton bean 'com.aspects.MyLoggingAspect#0'
647   [main] INFO  com.aspects.MyLoggingAspect  - method will be invoked:-void com.dao.MyDAO.add()
--DATA: 1
648   [main] INFO  com.aspects.MyLoggingAspect  - method will be invoked:-void com.dao.MyDAO.data(int,int)
--mymethod invoked
648   [main] INFO  com.aspects.MyLoggingAspect  - method will be invoked:-int com.dao.MyDAO.myMethod()
*/