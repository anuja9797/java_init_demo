import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

public class MessageReceiver {	//DEFAULT BROKER URL tcp://localhost:61616
	
	//DEFAULT BROKER URL tcp://localhost:61616
	
	
		private static String url=ActiveMQConnection.DEFAULT_BROKER_URL;
		//queue from where msgs will be received from
		private static String subject="JCG_QUEUE",subject2="MY_QUEUE";
		
		public static void main(String[] args) throws JMSException {
			
			//GET JMS CONNECTION FROM SERVER AND START IT
			ConnectionFactory connectionFactory =new ActiveMQConnectionFactory(url);
			Connection connection;
			
			connection = connectionFactory.createConnection();
			connection.start();
				
			//CREATE NON TRANSACTIONAL SESSION TO RECEIEVE JMS msg
			Session session=connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
				
			//destination represents my_queue on jms server. queue is created automatically
			Destination destination=session.createQueue(subject2);
			
			//to receive msgs
			MessageConsumer consumer=session.createConsumer(destination);
			Message msg = consumer.receive();
			
			if(msg instanceof TextMessage)
			{
				TextMessage txtmsg=(TextMessage)msg;
				System.out.println("Received msg: "+txtmsg.getText());
			}
			
			connection.close();
			
			
		}
}


