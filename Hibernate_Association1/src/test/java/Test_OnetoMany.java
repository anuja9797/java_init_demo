import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.beans.association.Address_One_to_Many;
import com.beans.association.Person_One_to_Many;

public class Test_OnetoMany {
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Configuration configuration = new Configuration().configure();
	    List<Address_One_to_Many> addresses=new ArrayList<Address_One_to_Many>();
		Person_One_to_Many p=new Person_One_to_Many();
		
		
			try(
				SessionFactory sessionFactory = configuration.buildSessionFactory();
				Session session = sessionFactory.openSession();)
			{
			
			session.beginTransaction();
			addresses.add(new Address_One_to_Many(404,"Dhule",456006l));
			addresses.add(new Address_One_to_Many(505,"Thane",96356l));
			addresses.add(new Address_One_to_Many(606,"Goa",91005l));
			p.setAddress(addresses);
			p.setName("Juilee");
			//select * from ANU_PEROTOM_ANU_ADDOTOM;   //Intermediate table
			session.save(p);
			session.getTransaction().commit();
			
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		
		
	}
}
