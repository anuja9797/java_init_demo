package com.dao;

//import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
@Component
public class MyDAO {

//	Logger logger=Logger.getLogger(getClass());

	public void add()
	{
//		logger.info("ADD METHOD INVOKED");
		System.out.println("--added");
	}
	public void data(int x,int y)
	{
//		logger.info("DATA INVOKED");
		if(x<0 || y<0)
		{
//			logger.error("x<0 or y<0",new Exception());
		}
		System.out.println("--DATA: "+(x+y));
	}
	public int myMethod() {
//		logger.info("myMethod method invoked");
		System.out.println("--mymethod invoked");
		int j=0;
		for(int i=0;i<100;i++)
		{
			j++;
		}
		return j;
	}
}
