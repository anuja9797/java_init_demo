package com.collection;

import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

public class MyAddressMap {

	Map<String,Address> addresses;

	public MyAddressMap() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((addresses == null) ? 0 : addresses.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MyAddressMap other = (MyAddressMap) obj;
		if (addresses == null) {
			if (other.addresses != null)
				return false;
		} else if (!addresses.equals(other.addresses))
			return false;
		return true;
	}
	public Map<String, Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(Map<String, Address> addresses) {
		this.addresses = addresses;
	}
	@Override
	public String toString() {
		Set<Entry<String,Address>> entryset=addresses.entrySet();
		String res="";
		for(Entry<String, Address> e:entryset)
			{
			res+="KEY: "+e.getKey()+" VALUE: ";
			res+=(e.getValue().toString()+"\n");
			}
		return res;
		//return "MyAddressMap [addresses=" + addresses + "]";
	}

	
	
}
