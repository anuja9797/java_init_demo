import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.beans.MyData;

public class DemoStream {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<MyData> list=new ArrayList<>();
		
		list.add(new MyData(1,"abc",1201));
		list.add(new MyData(2,"xyz",1201));
		list.add(new MyData(3,"pqr",1202));
		list.add(new MyData(4,"apr",1203));
		list.add(new MyData(1,"abc",1201));
		
//		System.out.println(list);
		
//		Iterator<MyData> itr=list.iterator();
//		while(itr.hasNext())
//		{
//			MyData d=itr.next();
//			if(d.getDataValue().charAt(0)=='a')
//				System.out.println(d);
//		}
		
		Stream<MyData> stream=list.stream();
//		stream.forEach(System.out::println);  //Using method reference operator ::

//		stream.forEach((data)->{System.out.println(data);});  //Using lambda expr
//		stream=list.stream();//Streams are not reusable hence, 
//		stream.forEach((data)->{System.out.println(data.getDataValue());});  //Using lambda expr
		
		//instead
//		list.stream().forEach((data)->{System.out.println(data);});
//		list.stream().forEach((data)->{System.out.println(data.getDataId());});

		
		//terminal op
		
		//1.filter
//		list.stream().filter((x)->{
////			System.out.println("Handling X:"+x);
//			return x.getDataValue().startsWith("a");}).forEach((d)->{System.out.println(d);});
		
		
		//OR
		Predicate<MyData> pred= (x) ->{
			return x.getDataValue().startsWith("a");
		};//return boolean
//		list.stream().filter(pred).forEach((a)->{System.out.println(a);}); //forEach is at service level that prints the data
		
		List<MyData> dlist=list.stream().filter(pred).collect(Collectors.toList());  //to collect the data in list
		dlist.stream().forEach(System.out::println);
		
		long counter=list.stream().filter(pred).count();
		System.out.println("\nCounter="+counter);
	}

}
