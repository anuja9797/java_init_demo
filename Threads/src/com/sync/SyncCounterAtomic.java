package com.sync;

import java.util.concurrent.atomic.AtomicInteger;

public class SyncCounterAtomic {
	private AtomicInteger value=new AtomicInteger(0);
	public void inc() {value.incrementAndGet();}
	public void dec() {value.decrementAndGet();}
	public int getValue() {return value.get();}
}
