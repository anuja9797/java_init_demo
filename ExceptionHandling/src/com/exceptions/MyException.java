package com.exceptions;

public class MyException extends Exception{

	String msg;
	public MyException() {
		// TODO Auto-generated constructor stub
		msg="EXCEPTION OCCURED!";
	}
	public MyException(String msg) {
		super(msg);//if we want to avoid getMessage or toString super()->super(msg)
		this.msg = msg;
	}
	public void display()
	{
		System.out.println(msg);
	}
//	@Override
//	public String toString() {
//		return "MyException [msg=" + msg + "]";//used by stacktrace
//	}
//	@Override
//	public String getMessage() {
//		return "msg:"+msg;	//used by stacktrace
//	}
	
}
