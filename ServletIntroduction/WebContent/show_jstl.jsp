<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="com.pojo.Employee"%>
<%@page import="java.util.List" %>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%
List<Employee> emplist1=(List<Employee>) request.getAttribute("emplist");
pageContext.setAttribute("list",emplist1);
%>

<br>
<table border="2">
<tr>
	<td><b>NAME</b></td>
	<td><b>ID</b></td>
	<td><b>AGE</b></td>
	<td><b>DEPT</b></td>
	</tr>
	<c:forEach var="e" varStatus="st" items="${list}">
	
	<tr>
	<td> <c:out value="${e.empName}" > </c:out></td>
	<td> <c:out value="${e.empId}" > </c:out></td>
	<td> <c:out value="${e.empAge}" > </c:out></td>
	<td> ${e.dept} </td>
	</tr>
	</c:forEach>
</table>
</body>
</html>