import java.util.Scanner;
import java.util.function.Predicate;

public class DemoInteger {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Predicate<Integer> pstr= s ->{
			return s>=0;
		};
		
				
		System.out.println(pstr.test(-5));
		System.out.println(pstr.test(0));
		System.out.println(pstr.test(8));
		System.out.println("Enter Integer: ");
		Scanner inp=new Scanner(System.in);
		int a=inp.nextInt();
		System.out.println(pstr.test(a));
	}
}
