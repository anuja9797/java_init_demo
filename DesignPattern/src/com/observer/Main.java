package com.observer;

import com.observer.MyModel.Data;

public class Main {

	public static void main(String[] args) {
		MyModel model=new MyModel();
		MyObserver obsv=new MyObserver(model);
		for(Data d:model.getData())
		{
			d.setName(d.getName()+" new");
		}
	}
	
	
}
