package com.sub_class;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@PrimaryKeyJoinColumn(name="empId")
@Table(name="Anu_WageEmployeeSubClass")
public class WageEmployee_SubClass extends Employee_SubClass{

	private int wages,hours;
	
	public WageEmployee_SubClass() {
		// TODO Auto-generated constructor stub
	}

	public WageEmployee_SubClass(int wages, int hours, String name, String address) {
		super (name, address);
		this.wages = wages;
		this.hours = hours;
		// TODO Auto-generated constructor stub
	}
	public int getWages() {
		return wages;
	}

	public void setWages(int wages) {
		this.wages = wages;
	}

	public int getHours() {
		return hours;
	}

	public void setHours(int hours) {
		this.hours = hours;
	}
	
}
