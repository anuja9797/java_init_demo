package com.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {

	
		// TODO Auto-generated constructor stub
		private static final SessionFactory sessionFactory=buildSessionFactory();
		private static SessionFactory buildSessionFactory()
		{
			try
			{
				return new Configuration().configure().buildSessionFactory();
			}catch(Exception e)
			{
				System.out.println("INITIAL SESSION FACTORY CREATION FAILED");
				e.printStackTrace();
				throw new ExceptionInInitializerError(e);
			}
			
		}
	
		public static SessionFactory getSessionKey()
		{
			return sessionFactory;
		}
		public static void shutdown()
		{	//closes caches and connection pools
			System.out.println("Shutdown called");
			getSessionKey().close();
		}

}
