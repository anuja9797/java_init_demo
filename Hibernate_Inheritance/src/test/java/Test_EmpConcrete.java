import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.concrete_class.Employee_Concrete;
import com.concrete_class.WageEmployee_Concrete;
import com.table_per_hierarchy.WageEmployee;

public class Test_EmpConcrete {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Configuration configuration = new Configuration().configure();
		  
		
		
		try(
			SessionFactory sessionFactory = configuration.buildSessionFactory();
			Session session = sessionFactory.openSession();)
		{
		
		session.beginTransaction();
		Employee_Concrete emp=new Employee_Concrete("Anuja","Pune");
		WageEmployee_Concrete wemp=new WageEmployee_Concrete(551,741,"Juilee","Dhule");
		session.save(emp);
		session.save(wemp);
		session.getTransaction().commit();
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}
