import java.lang.reflect.Method;
import java.lang.reflect.Type;

public class DemoReflection1 {

	public int display()
	{
		System.out.println("DISPLAYING HELLO!");
		return 0;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DemoReflection1 ref=new DemoReflection1();
		System.out.println("class name:"+ref.getClass());
		//^gives known info about class
		Class c1=ref.getClass();
		System.out.println(c1.getConstructors());
		System.out.println(c1.getCanonicalName());
		System.out.println(c1.getSimpleName());
		System.out.println(c1.getTypeName());
		System.out.println(c1.getDeclaredAnnotations());
		System.out.println(c1.getSuperclass());
		Method[] marr=c1.getDeclaredMethods();
		for(Method m:marr)
		{
			System.out.println(m);
			Type type=m.getGenericReturnType();
			System.out.println(type);
			
		}
		
		
		
	}

}
