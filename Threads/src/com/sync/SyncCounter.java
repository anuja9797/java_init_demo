package com.sync;

public class SyncCounter {
	private int value=0;
//	synchronized public void inc() {value++;}
//	synchronized public void dec() {value--;}
	public void inc() {value++;}
	public void dec() {value--;}
	public int getValue() {return value;}
}
