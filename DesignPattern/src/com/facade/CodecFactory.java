package com.facade;

public class CodecFactory {

	public static Codec extract(VideoFile file)
	{
		String type=file.getCodecType();
		if(type.equals("mp4"))
		{
			System.out.println("Codefactory: extracting mpeg audio");
			return new MPEG4CompressionCodec();
		}
		else
		{
			System.out.println("Codefactory: extracting other audio");
			return new CompressionCodec();
		}
	}
}
