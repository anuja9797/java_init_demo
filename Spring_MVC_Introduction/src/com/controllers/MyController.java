package com.controllers;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MyController {

		@RequestMapping(value="message.htm",method=RequestMethod.GET)
		public ModelAndView showMessage()
		{
			ModelAndView mav=new ModelAndView("show","message","welcome to spring mvc");
			System.out.println("SHOWMESSAGE INVOKED");
			return mav;
		}
}
