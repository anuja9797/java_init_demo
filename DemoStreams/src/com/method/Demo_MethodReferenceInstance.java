package com.method;

import java.util.Scanner;

public class Demo_MethodReferenceInstance {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
//		Demo_MethodReferenceInstance d=new Demo_MethodReferenceInstance();
//		Scanner inp=new Scanner(System.in);
//		while(true) {
//		System.out.println("\n1:square 2:squareroot ");
//		int choice=inp.nextInt();
//		System.out.println("\nEnter number:");
//		int x=inp.nextInt();
//		System.out.println(d.square(x));
//		System.out.println(d.square_root(x));
//		System.out.println("Result: "+d.perform(choice,x));
//		}
		
		Demo_MethodReferenceInstance instance=new Demo_MethodReferenceInstance();
		FindSquare findSquare=instance::square;		
		System.out.println(perform(findSquare,5));
		
		
		System.out.println(perform(instance::square_root,40));
		
		System.out.println(perform((n)->{return n*n*n;},4));
		
	}
	
	
	double square(double x)
	{
		return x*x;
	}
	double square_root(double x)
	{
		return Math.sqrt(x);
	}
//	static public double perform(int choice,int x)
//	{
//		Demo_MethodReferenceInstance d=new Demo_MethodReferenceInstance();
//		if(choice==1)
//			return d.square(x);
//		return d.square_root(x);
//	}
	static public double perform(FindSquare findsquare,int x)
	{
	return findsquare.function(x);
	//FindSquare=> Interface name
	//findsquare=> Interface instance
	//function=> Function in functional Interface
	}

	
}
