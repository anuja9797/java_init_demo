import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.beans.AnuMyClass;

public class TestMyClassGet {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Configuration configuration = new Configuration().configure();
		try (SessionFactory sessionFactory = configuration.buildSessionFactory();
			
			Session session = sessionFactory.openSession();) {
			session.beginTransaction();
			
			AnuMyClass myclass =(AnuMyClass)session.get(AnuMyClass.class,555);
			
			System.out.println(myclass);
			session.getTransaction().commit();
			System.out.println("SAVED");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

}
// select * from tab where tname like 'ANU%';
