package com.service;
import java.util.ArrayList;
import java.util.Date;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.beans.Person;

import com.dao.PersonDAOInterface;
import com.exception.PersonNotFoundException;

@Service //with this annotaion for this class create a bean

public class PersonService {

		List<Person> persons=new ArrayList<>();
		@Autowired
//		@Qualifier(value="anu_hib_impl")  //anu_hib_impl_jdbc
		PersonDAOInterface personDAO;
		
		public List<Person> getPersons() {
			return persons;
		}
		
		public void setPersons(List<Person> persons) {
		
			this.persons = persons;
		
		}
		
		public PersonService()
		{
			
			persons.add(new Person("A", 1,new Date()));
			persons.add(new Person("B", 2,new Date()));
			persons.add(new Person("C", 3,new Date()));
			
		}
		
		//business logic
		
		public List<Person> getAllPersons() {
		
			return personDAO.getAllPersons_1();
//			return personDAO.findAll();
		
		}
		
//		public Person getPerson(int id) {
//			for(Person p:persons)
//			{
//				if(p.getPersonId()==id)
//				{
//				System.out.println("service" +id);
//				return p;
//				}
//			}
//			return new Person();			
//		}
//		public Person getPerson(int id) {
//				return personDAO.findById(id).orElseThrow(()->new PersonNotFoundException("no person"));
//					
//		}
//		public List<Person> getPerson_name(String name) {
//			return personDAO.findByName(name);
//				
//	}
		public Person addPerson(Person p) {
		
			personDAO.addPerson(p);
			return personDAO.getPerson(p.getPersonId());
//			Person person=personDAO.save(p);
//			return person;
		}
		
		public Person deletePerson(int id) {
		
			Person p=new Person();
			p.setPersonId(id);
			if(persons.remove(p))
			{
				return p;
			}
			else
			{
				return null;
			}
		
		}

}