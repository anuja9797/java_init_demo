import java.util.TreeSet;
import java.util.Iterator;
import java.util.Set;

import com.beans.MyData;
import com.my_comparators.MyDataIdComparator;

public class DemoMyDataTreeSet {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Set<MyData> list=new TreeSet<MyData>();
		Set<MyData> list=new TreeSet<MyData>(new MyDataIdComparator());  //uses other comparator than default in MyObject
		list.add(new MyData(1,"abc",1201));
		list.add(new MyData(2,"xyz",1201));
		list.add(new MyData(3,"pqr",1202));
		list.add(new MyData(4,"apr",1203));
		list.add(new MyData(1,"abc",1201));
		
//		MyData obj=list.remove(3);//remove by index not available
//		System.out.println("\n\nREMOVED "+obj);
		
		
		MyData data=new MyData(1,"abc",1201);
		//list.add(data); if you add by reference and delete by reference..all objects of that ref  will be deleted
		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			MyData myData = (MyData) iterator.next();
			System.out.println(myData);
		}
		
		boolean b=list.remove(data);  //remove by value==> equals method NEEDED
		System.out.println("\n\nREMOVED "+data+" "+b);
		
		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			MyData myData = (MyData) iterator.next();
			System.out.println(myData);
		}
		
		System.out.println("\n CONTAINS  :"+list.contains(new MyData(4,"apr",1203)));
	}

}
