import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;

public class Demoproperties {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Properties prop=new Properties();
		try(InputStream input=Demoproperties.class.getClassLoader().getResourceAsStream("mydata.properties"))
		{
			//load properties file from class path
			prop.load(input);
			
			//get a property value
			System.out.println("Company Name: "+prop.getProperty("companyname"));
			
			//get All property value
			Enumeration<?> e=prop.propertyNames();
			while(e.hasMoreElements())
			{
				String key=(String) e.nextElement();
				String value=prop.getProperty(key);
				System.out.println("Key: "+key+"\t\tValue: "+value);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
