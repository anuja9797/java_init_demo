package com.pojo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Client {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ApplicationContext context=new ClassPathXmlApplicationContext("beans.xml");//load factory and create object
		System.out.println("IN MAIN");
		Person person=(Person)context.getBean( "per"); //per bean
		//bean name =>return Object
		//person.display();
		System.out.println(person);
		
		Person person1=(Person)context.getBean("per1"); //per1 bean
		System.out.println(person1);
		
		Person person2=(Person)context.getBean("per"); //per bean =>prototype=>New Object everytime
		System.out.println(person2);
		
//		System.out.println("after change");
//		person.setpName("NIKITA");
//		person.display();//per bean updated
//		person1.display();//per1 bean
//		person2.display();//per bean diff copy => not changed
		
		Person per_ns=(Person)context.getBean("per_ns");
		System.out.println(per_ns);
		
		
		Person per_ns_cons=(Person)context.getBean("per_ns_const2");
		System.out.println(per_ns_cons);
	}

}
