package com.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.pojo.Book;

public class BookMapper implements RowMapper {

	@Override
	public Book mapRow(ResultSet rs, int a) throws SQLException {
		// TODO Auto-generated method stub
		
		
		Book b=new Book();
		b.setBookName(rs.getString("bookname"));
		b.setISBN(rs.getLong("ISBN"));
		b.setPublication(rs.getString("publication"));
		b.setDescription(rs.getString("description"));
		b.setAuthor(rs.getString("author"));
		b.setPrice(rs.getInt("price"));
		return b;
	}


}
