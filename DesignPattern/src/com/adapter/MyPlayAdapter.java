package com.adapter;

public class MyPlayAdapter implements Toy {

	Bird bird;
	public MyPlayAdapter(Bird bird)
	{
		this.bird=bird;
	}
	
	@Override
	public void play() {
		// TODO Auto-generated method stub
		bird.makesound();
	}

	public static void main(String[] args) {
		
		Sparrow s=new Sparrow();
		TeddyBear t=new TeddyBear();
		
		
		Toy toy=new MyPlayAdapter(s);
//		Toy toy1=new MyPlayAdapter(s);//Do using anon inner class
		toy.play();
	}
	

}
