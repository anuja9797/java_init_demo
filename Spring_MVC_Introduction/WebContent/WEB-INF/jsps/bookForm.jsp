<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="bform" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<bform:form modelAttribute="book" method="POST" action="addBook.htm">
	<h2><center>ENTER DETAILS</center></h2>
	<table width="100%" height="150" align="center" border="0">
		<tr>
			<td align="right" width="50%">NAME</td>
			<td align="left" width="50%">
			<bform:input path="bookName" size="30"/>
			<font color="red"><bform:errors path="bookName"/></font>
		</tr>
		<tr>
			<td align="right" width="50%">ISBN</td>
			<td align="left" width="50%"><bform:input path="ISBN" size="30"/>
			<font color="red"><bform:errors path="ISBN"/></font>
		</tr>
		<tr>
			<td align="right" width="50%">AUTHOR</td>
			<td align="left" width="50%"><bform:input path="author" size="30"/>
			<font color="red"><bform:errors path="author"/></font>
		</tr>
		<tr>
			<td align="right" width="50%">PUBLICATION</td>
			<td align="left" width="50%"><bform:input path="publication" size="30"/>
			<font color="red"><bform:errors path="publication"/></font>
		</tr>
		<tr>
			<td align="right" width="50%">PRICE</td>
			<td align="left" width="50%" height="20">
			<bform:select path="price" size="1" >
				<bform:options items="${pricelist }"></bform:options>
			</bform:select>
			<font color="red"><bform:errors path="price"/></font>
		</tr>
		<tr>
			<td align="right" width="50%">DESCRIPTION</td>
			<td align="left" width="50%"><bform:input path="description" size="30"/>
			<font color="red"><bform:errors path="description"/></font>
		</tr>
		<tr>
			<td colspan="2" align="center">
				<input type="submit" value="ADD BOOK"/>
			</td>
		</tr>
	</table>
	</bform:form>
	
</body>
</html>