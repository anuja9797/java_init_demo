import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.beans.AnuMyClass;

public class TestMyClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Configuration configuration = new Configuration().configure();
		try (SessionFactory sessionFactory = configuration.buildSessionFactory();
				/*
				 * ServiceRegistry serviceRegistry=new
				 * StandardServiceRegistryBuilder().applySettings(configuration.getProperties())
				 * .build(); SessionFactory
				 * sessionFactory=configuration.buildSessionFactory(serviceRegistry);
				 */
				Session session = sessionFactory.openSession();) {

			session.beginTransaction();
			AnuMyClass myclass = new AnuMyClass();
			myclass.setClassId(27);
			myclass.setClassName("demo1");
			myclass.setValue(155d);
			session.save(myclass);//saveOrUpdate preferred
			session.getTransaction().commit();
			System.out.println("SAVED");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

}
// select * from tab where tname like 'ANU%';
