package com.table_per_hierarchy;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value="wageemployee")
public class WageEmployee extends Employee {
	
	private int wages,hours;
	
	public WageEmployee() {
		// TODO Auto-generated constructor stub
	}

	public WageEmployee(int wages, int hours, String name, String address) {
		super (name, address);
		this.wages = wages;
		this.hours = hours;
		// TODO Auto-generated constructor stub
	}



	public int getWages() {
		return wages;
	}

	public void setWages(int wages) {
		this.wages = wages;
	}

	public int getHours() {
		return hours;
	}

	public void setHours(int hours) {
		this.hours = hours;
	}
	
	

}
