import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.lang.reflect.Type;

import com.pojo.MyPerson;

public class DemoPerson {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		MyPerson ref=new MyPerson();
	
		Class obj=ref.getClass();
		System.out.println("Classname: "+ref.getClass());
		System.out.println("Fields: ");
		
		Field fields[]=obj.getDeclaredFields();
		for(Field f:fields)
			{
			System.out.print("Name: "+f.getName()+"\tType: "+f.getType().getName());
			if(f.getModifiers()==Modifier.PRIVATE)
				System.out.println("\tModfier: PRIVATE");
			else if(f.getModifiers()==Modifier.PUBLIC)
				System.out.println("\tModfier: PUBLIC");
			}
		
		Constructor[] conts =obj.getConstructors();
		for(Constructor c:conts)
		{
			System.out.println();
			Parameter params[]=c.getParameters();
			for(Parameter p:params)
			{
				System.out.print(p.getParameterizedType().getTypeName()+"\t");
			}
		}
		
		System.out.println();
		Method[] marr=obj.getDeclaredMethods();
		for(Method m:marr)
		{
			Type type=m.getGenericReturnType();
			System.out.println(m.getName()+"\t\t"+type.getTypeName());
			
		}
		Method call_display;
		try {
			System.out.println("Get method Display:");
			
			call_display = obj.getDeclaredMethod("display",null);//null=>no parameter
			System.out.println("\nInvoke display()");
			System.out.println(call_display.invoke(ref));
			
			
			Method call_display1 = obj.getDeclaredMethod("add",null);
			call_display1.setAccessible(true);//can access private method
			System.out.println("\nInvoke add()");
			System.out.println(call_display1.invoke(ref));
			
			Method call_display2 = obj.getDeclaredMethod("add",new Class[] {int.class,int.class});//add(int,int)
			call_display2.setAccessible(true);//can access private method
			System.out.println("\nInvoke add(int,int)");
			System.out.println(call_display2.invoke(ref,100,300));
			
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
