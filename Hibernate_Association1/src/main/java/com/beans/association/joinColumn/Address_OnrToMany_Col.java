package com.beans.association.joinColumn;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Anu_AddOtoMCol")
public class Address_OnrToMany_Col {
@Id
private int id;
private Long pincode;
private String city;

public Address_OnrToMany_Col() {
	// TODO Auto-generated constructor stub
}

public Address_OnrToMany_Col(int id, String city, Long pincode) {
	super();
	this.id = id;
	this.pincode = pincode;
	this.city = city;
}

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public Long getPincode() {
	return pincode;
}

public void setPincode(Long pincode) {
	this.pincode = pincode;
}

public String getCity() {
	return city;
}

public void setCity(String city) {
	this.city = city;
}

@Override
public String toString() {
	return "Address_One_to_Many [id=" + id + ", pincode=" + pincode + ", city=" + city + "]";
}


}
