package com.pojo;

public class Person {
	
	private int pId;
	private String pName;
	public Person() {
		// TODO Auto-generated constructor stub
		System.out.println("DEFAULT CONSTRUCTOR");
	}
	
	public void setpId(int pId) {
		this.pId = pId;
	}

	public Person(int pId, String pName) {
		System.out.println("PARAM CONSTRUCTOR");
		this.pId = pId;
		this.pName = pName;
	}

	public void setpName(String pName) {
		this.pName = pName;
	}

	public void display()
		{
			System.out.print("DISPLAYING PERSON ");
			System.out.println("PID: "+pId+" NAME: "+pName);
		}
	@Override
	public String toString() {
		return "person [pId=" + pId + ", pName=" + pName + "]";
	}
}
