package com.pojo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Client_contain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		@SuppressWarnings("resource")
		ApplicationContext context=new ClassPathXmlApplicationContext("wiring.xml");
//		MyDemo demo=(MyDemo) context.getBean("myDemo");//automatic decapitalized class name by default
		MyDemo demo=(MyDemo) context.getBean("mydemo"); //Component(value="mydemo")
		demo.display();
		
		Address add=(Address) context.getBean("myadd");
		System.out.println(add);
		
		Employee emp=(Employee)context.getBean("employee");
		System.out.println(emp);
	}

}
