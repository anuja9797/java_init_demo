package com.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyRestController {
	@GetMapping("/message")
	public String showMessage() {
		System.out.println("hello");
		return "thismesage";
	}

	@GetMapping("/hello")
	@Secured("hasRole('ROLE_MANAGAER')")
	public String showHello() {
		System.out.println("show hello");
		return "hello";
	}

}
