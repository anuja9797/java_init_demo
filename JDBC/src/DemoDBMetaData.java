import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

public class DemoDBMetaData {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			System.out.println("DRIVER LOADED");	
			Connection conn=DriverManager.getConnection("jdbc:oracle:thin:@vhkdld387:1551:staffd","hr","Eagle#2019"); //url, username, pwd
			System.out.println("Connected");
			
			DatabaseMetaData meta=conn.getMetaData();
			System.out.println("\nPRODUCT VERSION:"+meta.getDatabaseProductVersion());
			System.out.println("\nPRODUCT NAME:"+meta.getDatabaseProductName());
			System.out.println("\nDRIVER VERSION:"+meta.getDriverVersion());
			System.out.println("\nDEIVER NAME:"+meta.getDriverName());
			
			
			String FIND_ALL_BOOKS=" SELECT * FROM ANU_BOOK";
			PreparedStatement ps=conn.prepareStatement(FIND_ALL_BOOKS);
			ResultSet rs=ps.executeQuery();
			while(rs.next())
			{
				int isbn=rs.getInt(1);
				String name=rs.getString("name");
				String author=rs.getString(3);
				int price=rs.getInt(4);
				System.out.println(isbn+"\t\t"+name+"\t\t"+author+"\t\t"+price);
			}
			
			ResultSetMetaData res_meta=rs.getMetaData();
			for(int i=1;i<=res_meta.getColumnCount();i++)
			{
				System.out.println("\n\n"+res_meta.getColumnLabel(i)+"\t"+res_meta.getColumnType(i)+"\t"+res_meta.getColumnTypeName(i)+"\t"+res_meta.getColumnClassName(i));
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
