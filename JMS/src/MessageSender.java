import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

public class MessageSender {
	private static String url=ActiveMQConnection.DEFAULT_BROKER_URL;
	private static String subject="MY_QUEUE",subject2="MY_QUEUE2";
	public static void main(String[] args) throws JMSException {
		
		//GET JMS CONNECTION FROM SERVER AND START IT
		ConnectionFactory connectionFactory =new ActiveMQConnectionFactory(url);
		Connection connection;
		
		connection = connectionFactory.createConnection();
		connection.start();
			
		//CREATE NON TRANSACTIONAL SESSION TO SEND/RECIEVE JMS msg
		Session session=connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			
		//destination represents my_queue on jms server. queue is created automatically
		Destination destination=session.createQueue(subject);
		
		//used to send msgs
		MessageProducer producer=session.createProducer(destination);
		TextMessage msg=session.createTextMessage("HELLO WORLD!");
		
		
		producer.send(msg);
		System.out.println("printing@@ "+msg.getText());
		//msg is registered on jms 
		
		destination=session.createQueue(subject2);
		producer=session.createProducer(destination);
		msg=session.createTextMessage("HELLO WORLD! FROM ANOTHER QUEUE");
		producer.send(msg);
		System.out.println("printing@@ "+msg.getText());
		
		connection.close();
		}
	
}
