package com.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code=HttpStatus.NOT_FOUND)
public class PersonNotFoundException extends RuntimeException {

	String message;
	
	public PersonNotFoundException(String message,Throwable cause,boolean enableSuppression,boolean writableStackTrace)
	{
		super(message,cause,enableSuppression,writableStackTrace);
	}
	public PersonNotFoundException(String msg,Throwable cause)
	{
		super(msg,cause);
	}
	public PersonNotFoundException(String msg)
	{
		super(msg);
	}
	public PersonNotFoundException(Throwable cause)
	{
		super(cause);
	}
}
