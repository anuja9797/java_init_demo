package com.factory;

public class CurrencyFactory {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
//		System.out.println(createCurrency("india").getCurrency());
//		System.out.println(createCurrency("US").getCurrency());
//		System.out.println(createCurrency("Tanzania").getCurrency());
		//Run-Config>Arguments>Apply>Run
		Currency c;
		String country=args[0];
		c=createCurrency(country);
		System.out.println(c.getCurrency());
	}

	public static Currency createCurrency(String country)
	{
		if(country.equalsIgnoreCase("india"))
			return new Curr_Rupees();
		else if(country.equalsIgnoreCase("us"))
			return new Curr_Dollar();
		else if(country.equalsIgnoreCase("tanzania"))
			return new Curr_TSH();
		throw new IllegalArgumentException("NO SUCH CURRENCY");
	}
	
}
