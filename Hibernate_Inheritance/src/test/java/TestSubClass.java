import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.sub_class.Employee_SubClass;
import com.sub_class.WageEmployee_SubClass;

public class TestSubClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Configuration configuration = new Configuration().configure();
		try(
			SessionFactory sessionFactory = configuration.buildSessionFactory();
			Session session = sessionFactory.openSession();)
		{
		
		session.beginTransaction();
		//Employee_SubClass emp=new Employee_SubClass("Anuja","Pune");
		WageEmployee_SubClass wemp=new WageEmployee_SubClass(551,741,"Juilee","Dhule");
		session.save(wemp);
		session.getTransaction().commit();
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}
/*
Hibernate: 
create table Anu_EmployeeSubClass (
       empId number(10,0) not null,
        address varchar2(255 char),
        name varchar2(255 char),
        primary key (empId)
    )
create table Anu_WageEmployeeSubClass (
   hours number(10,0) not null,
    wages number(10,0) not null,
    empId number(10,0) not null,
    primary key (empId)
)
alter table Anu_WageEmployeeSubClass 
       add constraint FKll9fnufu2q60kg7385haej0vn 
       foreign key (empId) 
       references Anu_EmployeeSubClass
*/