import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.beans.association.joinColumn.Address_OnrToMany_Col;
import com.beans.association.joinColumn.Person_OneToMany_Col;

public class Test_OnetoMany_Remove {	public static void main(String[] args) {
	// TODO Auto-generated method stub

	Configuration configuration = new Configuration().configure();
    List<Address_OnrToMany_Col> addresses=new ArrayList<Address_OnrToMany_Col>();
    Person_OneToMany_Col p=new Person_OneToMany_Col();
	
	
		try(
			SessionFactory sessionFactory = configuration.buildSessionFactory();
			Session session = sessionFactory.openSession();)
		{
		
		session.beginTransaction();
		Person_OneToMany_Col person=(Person_OneToMany_Col)session.get(Person_OneToMany_Col.class, 1);
		System.out.println(person);
		
		//person.getAddress().remove(0);	

		/*   //Removes only person_id from FIRST(0) address table..and not record in the address table
		 
    		update Anu_AddOtoMCol  set person_id=null  where person_id=? and id=?
		 */
		
		
		session.delete(person);
		
		/*
	//deletes person_id from address,delete address rows and person rows
	 
    update Anu_AddOtoMCol set  person_id=null where person_id=?
	delete  from  Anu_AddOtoMCol where id=?
	delete from  Anu_AddOtoMCol  where id=?
	delete from  Anu_AddOtoMCol  where id=?
	delete from Anu_PerOtoM_Col  where personId=?
		 */
		session.getTransaction().commit();
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	
	
}}
