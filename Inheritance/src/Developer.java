
public class Developer extends Employee{

	private String  comp_name;
	private String projName;
	
	public Developer()
	{
		comp_name="CLSA";
		projName="CC Update";
		//System.out.println("DEV CONSTRUCTOR DEF");
		//Default parent constructor implicitly called
	}

	public Developer(String comp_name, String projName) {
		//Default parent constructor implicitly called
	//super(); ==>explicit(no need)
	//System.out.println("DEV CONSTRUCTOR PARAM");
	this.comp_name = comp_name;
	this.projName = projName;
			
	}
	
	public Developer(int empid, String name,String comp_name, String projName) {
		
	super(empid, name);
	//System.out.println("DEV CONSTRUCTOR PARAM BOTH");
	this.comp_name = comp_name;
	this.projName = projName;
	// TODO Auto-generated constructor stub
	}
	
	
@Override
	public boolean equals(Object arg0) {
		// TODO Auto-generated method stub
		Developer d=(Developer)arg0;
		return super.equals(new Employee(d.getEmpid(),d.getName())) && this.projName.equals(d.projName) && this.comp_name.equals(d.comp_name);
		
	}

		public void Display()
	{
		System.out.println(comp_name+"\t"+projName);
	}
		public void Display(String name)//overloading
		{
			System.out.println(comp_name+"\t"+projName);
		}
		
		
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString()+" Developer [comp_name=" + comp_name + ", projName=" + projName + "]";
		//super.toString() avoids recursion of toString by resolving scope conflict
	}
	
	
	public String getComp_name() {
		return comp_name;
	}
	
	public void setComp_name(String comp_name) {
		this.comp_name = comp_name;
	}
	
	public String getProjName() {
		return projName;
	}
	public void setProjName(String projName) {
		this.projName = projName;
	}

	public static void MyData()
	{
		System.out.println("STATIC METHOD DEV");
	}
	//cannot be overridden
}
