import java.util.Locale;
import java.util.ResourceBundle;

public class DemoResourceBundle {
public static void main(String[] args) {
	
	Locale loc=new Locale("es","Spain");
	ResourceBundle rb=ResourceBundle.getBundle("message",loc);
	System.out.println(rb.getString("greeting"));
	
	loc=new Locale("fr","CA");
	rb=ResourceBundle.getBundle("message",loc);
    System.out.println(rb.getString("greeting"));
    
    loc=new Locale("");
	rb=ResourceBundle.getBundle("message",loc);
    System.out.println(rb.getString("greeting"));
    
    loc=new Locale("hin");
	rb=ResourceBundle.getBundle("message",loc);
    System.out.println(rb.getString("greeting"));
}
}
