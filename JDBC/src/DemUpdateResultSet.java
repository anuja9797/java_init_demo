import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

public class DemUpdateResultSet {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}  //not auto closable =>hence, try catch ClassNotFound and cannot be included in try()catch{};
		
		System.out.println("DRIVER LOADED");	
		
		String FIND_ALL_BOOKS=" SELECT ISBN,NAME,AUTHOR,PRICE FROM ANU_BOOK";
		try (
				Connection conn=DriverManager.getConnection("jdbc:oracle:thin:@vhkdld387:1551:staffd","hr","Eagle#2019"); //url, username, pwd
				Statement ps=conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
				ResultSet rs=ps.executeQuery(FIND_ALL_BOOKS);
			){
			
			System.out.println("CONNECTED");
			
//			int concurrency = rs.getConcurrency();
//            if (concurrency == ResultSet.CONCUR_UPDATABLE) {
//            	rs.moveToInsertRow();
//    			rs.updateInt("isbn", 28);
//    			rs.updateString("NAME", "Hibernate 5.0");
//    			rs.updateString("Author", "hibernate author");
//    			rs.updateInt("price", 235);
//    			rs.insertRow();                                                  //insert
//    			rs.moveToCurrentRow();
//            } else {
//            System.out.println("ResultSet is not an updatable result set.");
//            }
//            ps=conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
//            rs=ps.executeQuery(FIND_ALL_BOOKS);
			
			
			  rs.first();
			while(rs.next())
			{
				int isbn=rs.getInt(1);
				if(isbn==115)
				{
					System.out.println("UPDATED");
					rs.updateInt("price", 545);                                      //update
					rs.updateRow();
				}
			}
            
           rs.first();
            while(rs.next())
			{
            	int isbn=rs.getInt(1);
				if(isbn==28)
				{
					rs.deleteRow();                                                     //delete
				}
			}
            
            
                                                                                         //display
            rs.first();
            while(rs.next())
			{
				int isbn=rs.getInt(1);
				String name=rs.getString("name");
				String author=rs.getString(3);
				int price=rs.getInt(4);
				System.out.println(isbn+"\t\t"+name+"\t\t"+author+"\t\t"+price);
			}
//            ps.close();
//            conn.close();          //auto closed
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
