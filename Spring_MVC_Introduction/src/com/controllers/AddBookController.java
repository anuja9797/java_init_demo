package com.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.dao.BookDAO;
import com.pojo.Book;
@Controller
public class AddBookController {

	
	@Autowired
	@Qualifier(value="BookDAO_JdbcTemplate")
	BookDAO bookDAO;
	
	@RequestMapping("/showBookForm.htm")
	public ModelAndView ViewForm(ModelMap map)
	{
		Book book=new Book();
		map.addAttribute(book);
		System.out.println(book);
		return new ModelAndView("bookForm");
		
	}
	
	@RequestMapping("/addBook.htm")
	public ModelAndView AddBook(@Valid @ModelAttribute("book") Book book, BindingResult result)
	{
		
		if(result.hasErrors())
		{
			return new ModelAndView("bookForm");
		}
		bookDAO.addBook(book);
		List<Book> books=new ArrayList<Book>();
		books.add(book);
		
		ModelAndView mav=new ModelAndView();
		mav.setViewName("display");
		mav.addObject("book_list",books);
		mav.addObject("auth_name",book.getAuthor());
		return mav;
		
	}
	@ModelAttribute("pricelist")
	public List<Integer> addPrices()
	{
		List<Integer> price=new ArrayList<Integer>();
		price.add(250);			price.add(500);
		price.add(750);			price.add(1000);
		price.add(1250);		price.add(1500);
		price.add(1750);		price.add(2000);
		return price;
	}
	
	@Autowired
	Validator validator;
	
	@InitBinder
	private void initBinder(WebDataBinder webDataBinder)
	{
		webDataBinder.setValidator(validator);
	}
}
