package com.decorator;

public class CheeseDecorator extends SandwichDecorator{

	Sandwich current;
	public CheeseDecorator(Sandwich s) {
		// TODO Auto-generated constructor stub
		current=s;
	}
	@Override
	public Long price() {
		// TODO Auto-generated method stub
		return current.price()+200;
	}
	public Sandwich getCurrent() {
		return current;
	}
	
}
