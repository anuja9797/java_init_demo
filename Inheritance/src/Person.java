

public class Person {
	private String name;
	private int age;
	private Address address;
	public Person() {
		// TODO Auto-generated constructor stub
		age=20;
		name="name";
		address=new Address("City","pin");  //initialize
	}
	public Person(String name, int age,Address address) {
		this.name = name;
		this.age = age;
		this.address=address;
	}
	public Person(String name, int age,String city, String pincode) {
		this.name = name;
		this.age = age;
		this.address=new Address(city,pincode);
	}
	@Override
	public String toString() {
		return "Person [name=" + name + ", age=" + age + ", address=" + address + "]";
	}
	
}
