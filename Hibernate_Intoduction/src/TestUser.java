import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.beans.User;

public class TestUser {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Configuration config=new Configuration().configure();
		try(
				SessionFactory sessionfactory=config.buildSessionFactory();
				Session session=sessionfactory.openSession();
				)
		{
			session.beginTransaction();
			User user=new User();
			user.setCreatedBy("admin");
			user.setUserId(2);
			user.setUsername("ANUJA");
			user.setCreatedDate(new Date());
			user.setSqlDate(java.sql.Date.valueOf("2017-11-15"));
			session.save(user);
			session.getTransaction().commit();
			System.out.println("CLOSE");
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

}
