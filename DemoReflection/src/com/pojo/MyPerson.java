package com.pojo;

import java.util.Arrays;

public class MyPerson {
private int pid;
private String name;
private String[] hobbies;
public MyPerson() {
	// TODO Auto-generated constructor stub
}
public MyPerson(int pid, String name, String[] hobbies) {
	super();
	this.pid = pid;
	this.name = name;
	this.hobbies = hobbies;
}
public MyPerson(int pid, String name) {
	super();
	this.pid = pid;
	this.name = name;
}
public MyPerson(int pid, String[] hobbies) {
	super();
	this.pid = pid;
	this.hobbies = hobbies;
}
@Override
public String toString() {
	return "MyPerson [pid=" + pid + ", name=" + name + ", hobbies=" + Arrays.toString(hobbies) + "]";
}
public int getPid() {
	return pid;
}
public void setPid(int pid) {
	this.pid = pid;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String[] getHobbies() {
	return hobbies;
}
public void setHobbies(String[] hobbies) {
	this.hobbies = hobbies;
}
public void display()
{
	System.out.println("PID: "+pid+"\tName: "+name+"\thobbies: "+hobbies);
}
private int add()
{
	return 0;
}
private int add(int a,int b)
{
	return a+b;
}
}
