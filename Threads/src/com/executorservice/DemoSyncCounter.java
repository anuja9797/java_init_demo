package com.executorservice;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import com.sync.SyncCounter;

public class DemoSyncCounter {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		
		
		int threadnum=10;
		int itr=100;
		final SyncCounter cntr=new SyncCounter();
		
		Runnable r=()->
		{
			//run method
			
			System.out.println(Thread.currentThread().getName()+" start");
			for(int i=0;i<itr;i++) {
//			System.out.println(Thread.currentThread().getName()+"---"+i);
			cntr.inc();
//			System.out.println(Thread.currentThread().getName()+"***"+i);
			cntr.dec();
			
			}
			System.out.println(Thread.currentThread().getName()+" end");
		};
		
		ExecutorService service=Executors.newFixedThreadPool(threadnum);
		for(int i=0;i<threadnum;i++)
		{
			service.execute(r);
		}
//		service.shutdown();
//		service.awaitTermination(5, TimeUnit.SECONDS);//waits for termination of all threads and avoids abrupt termination
		service.shutdown();
		System.out.println(cntr.getValue());//different value everytime due to abrupt termination
		

	}

}
