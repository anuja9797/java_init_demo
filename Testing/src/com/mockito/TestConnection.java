package com.mockito;

//import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class TestConnection {

	
	@InjectMocks
	DemConnection democonn;
	@Mock
	Connection mock_conn;
	@Mock
	Statement mockstmt;
	@Mock
	PreparedStatement mockpstmt;
	
	@Before
	public void setUp() throws Exception {
		
		MockitoAnnotations.initMocks(this);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetConnection() throws SQLException {
	Mockito.when(mock_conn.createStatement()).thenReturn(mockstmt);
//  Mockito.when(mock_conn.createStatement()).thenCallRealMethod();
	Mockito.when(mock_conn.createStatement().executeUpdate(Mockito.any())).thenReturn(1);
	
	int value=democonn.getRecord("");
	Assert.assertEquals(value,1);
	Mockito.verify(mock_conn.createStatement(),Mockito.times(1));
	}

//	@Test
//	public void testGetRecord() {
//		//fail("Not yet implemented");
//	}

}
