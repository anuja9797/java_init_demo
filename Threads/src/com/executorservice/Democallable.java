package com.executorservice;

import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Democallable {
public static void main(String[] args) {
	// TODO Auto-generated method stub
	//ExecutorService service=Executors.newFixedThreadPool(3);
	ExecutorService service=Executors.newCachedThreadPool();
	Callable c=()->{
		System.out.println("I'm Callable  "+Thread.currentThread().getName());
		return new Random().nextInt(3);
	};
	for(int i=0;i<15;i++) {
	Future future=service.submit(c);
	System.out.println("future:  "+future);
	
	
	try {
		System.out.println(future.get()); //returns null=>successful completion
	} catch (InterruptedException | ExecutionException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}
	service.shutdown();
}
}
