
public class Student {
private int rollNo,age;
private String name;
private int[] marks;
private static int cnt;

static
{  //initialization using static block
	//cnt=0; 
	System.out.println("Initializing Count");
}


public Student() {
	//cnt++;	
	this.marks=new int[3];
	System.out.println("Def constructor");
}

public Student(int rollNo, int age, String name,int[] marks) {
	super();
	this.rollNo = rollNo;
	this.age = age;
	this.name = name;
	cnt++;
	this.marks=marks;
	
	//this.marks[0]=100;
   
}
public Student copyS()
{
	Student s=new Student(this.rollNo,this.age,this.name,this.marks);
	return s;
}
public static int getCnt() {
	//acessing static dm using static method
	return cnt;
}

public int getRollNo() {
	return rollNo;
}
public void setRollNo(int rollNo) {
	this.rollNo = rollNo;
}
public int getAge() {
	return age;
}
public void setAge(int age) {
	this.age = age;
}
public String getName() {
	return name;
}
@Override
protected void finalize() throws Throwable {
	// TODO Auto-generated method stub
	super.finalize();
	System.out.println("Finalize called");
}

public void setName(String name) {
	this.name = name;
}
public int[] getMarks() {
	return marks;
}
public void setMarks(int[] marks) {
	this.marks = marks;
}
public void Display()
{
	System.out.println("\nRollno: "+rollNo+" Name: "+name+" age: "+age+" marks:");
	for( int m:marks)
		System.out.print(m+"\t");
}




}
