import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.pojo.Person;

public class DemoCRUD {
	static int cnt=0;
	public static void main(String[] args) throws ClassNotFoundException {
		try {
			String filename="C:\\Users\\watpadea\\eclipse-workspace\\ExceptionHandling\\src\\person1.txt";
			
			
//			System.out.println((Person)oi.readObject());
//			Person person=new Person("Anuja",22,"Pune");
//			oo.writeObject(person);
//			System.out.println("OBJECT WRIITEN");
			Scanner inp=new Scanner(System.in);
			int choice=0;
			List<Person> list=new ArrayList<>();
			do {
			System.out.println("1:Add 2:Display 3:Update 4:Delete 5:Exit");
			choice=inp.nextInt();
			switch(choice)
			{
			case 1:
				ObjectOutputStream oo=new ObjectOutputStream(new FileOutputStream(filename));
				System.out.println("Enter Name");
				String name=inp.next();
				System.out.println("Enter Age");
				int age=inp.nextInt();
				System.out.println("Enter Address");
				String addr=inp.next();
				Person person=new Person(name,age,addr);
				
				System.out.println("Person Added");
				list.add(person);
				System.out.println(list);
				oo.writeObject(list);
				oo.close();
				break;
			case 2:
				ObjectInputStream oi=new ObjectInputStream(new FileInputStream(filename));
				Person obj=null;
				List<Person> list1;
				list1= (List<Person>) oi.readObject();
				for(Person value:list1)
				{
					System.out.println(value);
				}
				oi.close();
				break;
			case 3:
				System.out.println("Enter Name:");
				String name1=inp.next();
				System.out.println("Enter new Address:");
				String add1=inp.next();
				for(Person value:list)
				{
					if(value.getPersonName().equals(name1))
						value.setAddress(add1);
				}
				ObjectOutputStream oo1=new ObjectOutputStream(new FileOutputStream(filename));
				oo1.writeObject(list);
				oo1.close();
				break;
			case 4:

				System.out.println("Enter Name:");
				String name2=inp.next();
				int i=0,flag=0;
				for(Person value:list)
				{
					if(value.getPersonName().equals(name2))
						{ flag=1;
						 System.out.println(i);
						 break;
						 }
					i++;
				}
				if(flag==1)
					list.remove(i);
				ObjectOutputStream oo3=new ObjectOutputStream(new FileOutputStream(filename));
				oo3.writeObject(list);
				oo3.close();
				break;
			}
			}while(choice!=5);
			
		} catch (FileNotFoundException |ClassNotFoundException e) 
		//OR operator only when exceptions dont have parent-child relation
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} 
		
		
	}
}
