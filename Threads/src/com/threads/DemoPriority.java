package com.threads;

public class DemoPriority extends Thread {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("start");
		System.out.println("Thread : "+Thread.currentThread().getName());
		
		Thread t=new MyThread1();
		t.setName("t_minthread_anu");
		t.setPriority(Thread.MIN_PRIORITY);//10
		t.start();
		
		Thread t2=new MyThread1();
		t2.setName("t2_9thread_anu");
		t2.setPriority(9);
		t2.start();
		
		Thread t3=new MyThread1();
		t3.setName("t3_6thread_anu");
		t3.setPriority(6);
		t3.start();
	
		Thread t4=new MyThread1();
		t4.setPriority(Thread.MAX_PRIORITY);
		t4.setName("t4_Thread.max_PRIORITY");
		t4.start();
	
		try {
			t4.join();
			t3.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("end");
	}
	@Override
	public void run()
	{
		System.out.println("Thread in RUN: "+Thread.currentThread().getName());
	}
}
