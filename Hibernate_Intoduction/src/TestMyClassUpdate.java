import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.beans.AnuMyClass;

public class TestMyClassUpdate {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Configuration configuration = new Configuration().configure();
		try (SessionFactory sessionFactory = configuration.buildSessionFactory();
			
			) {
			
			Session session = sessionFactory.openSession();
			session.beginTransaction();	//entity attached
			AnuMyClass myclass =(AnuMyClass)session.get(AnuMyClass.class,555);
			System.out.println(myclass);
			myclass.setValue(222d);
			session.getTransaction().commit();		//entity detached
			//Before commit no need to manually update
			
//			session.beginTransaction();
//			myclass.setValue(300d);
//			System.out.println(myclass);
//			session.saveOrUpdate(myclass);//detached entity cannot be updated hence,transaction is begin
//			session.getTransaction().commit();	
			
			Session session1 = sessionFactory.openSession();
			session1.beginTransaction();
			myclass.setValue(300d);
			System.out.println(myclass);
//			session1.save(myclass);//Gives UNIQUE CONSTRAINT exception
			session1.saveOrUpdate(myclass);//SAVEORUPDATE when two connections are accessing SAME OBJECT
			session1.getTransaction().commit();	
			System.out.println("SAVED");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

}
// select * from tab where tname like 'ANU%';
