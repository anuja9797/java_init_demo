<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<% 
		String message=(String) request.getAttribute("msg");
		if(message!=null)
		out.println(message);
		else
		message="default";
		pageContext.setAttribute("msg",message);
	%>
	<br><br>
	
	Using expression <%=message %>  <!-- prints null -->
	<br>
	USING EL <c:out value="${msg}"></c:out><br><br> <!-- Do not prints null -->
	USING EL ${msg}<br><br> <!-- Do not prints null -->
	FILL THE EMPLOYEE FORM
	<!-- form action="add" method="get"-->
	<form action="addJSP.jsp" method="get"><br>
		Employee Id:<input type="text" name="empId"><br>
		EmpName:<input type="text" name="empName"><br>
		EmpAge:<input type="text" name="empAge"><br>
		Address:<input type="text" name="empAddress"><br>
		Department:<input type="text" name="dept"><br>
		Experience:<input type="text" name="empExperience"><br>
		
		<input type="submit" value="ADD EMPLOYEE">
	</form>
</body>
</html>