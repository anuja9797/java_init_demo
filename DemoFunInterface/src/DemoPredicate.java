import java.util.function.Predicate;

public class DemoPredicate {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Predicate<String> pstr= s ->{
			return s.equals("hello");
		};
		
		boolean value=pstr.test("CLSA");
		if(value)
			System.out.println("EQUAL");
		else
			System.out.println("NOT EQUAL");
		
		System.out.println(pstr.test("hello"));
		System.out.println(pstr.test("Hello"));
		System.out.println("----------------------------");
		Predicate<String> pstrA= pstr.and(
			s1 ->{
				System.out.println("AND TEST INVOKE");
				return s1.length()>=5;
			}
		);
		System.out.println(pstrA.test("hello"));
		System.out.println(pstrA.test("Hello"));
		
		System.out.println("----------------------------");
		Predicate<String> pstrOR= pstr.or(
			s1 ->{
				System.out.println("condn 1 false => OR TEST INVOKE");
				return s1.length()>=5;
			}
		);
		System.out.println(pstrOR.test("hello"));
		System.out.println(pstrOR.test("Hello"));
	}

}
