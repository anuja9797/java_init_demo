import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DemoList {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static void main(String[] args) {
		// TODO Auto-generated method stub

			List intlist=new ArrayList();
			intlist.add(100);intlist.add(200);intlist.add(300);intlist.add(400);intlist.add(100);
			System.out.println(intlist.size());
			System.out.println(intlist);
			
//			Integer obj=new Integer(99);
//			intlist.add(5,obj);			 //BOXING-UPCASTING
//			intlist.add(5,"new");
//			intlist.add(5,new Thread());
//			intlist.add(5,1.02f);
			
			Iterator itr=intlist.iterator();
			while(itr.hasNext())
			{
				Integer i=(Integer)itr.next();//Downcasting
				if(i>100) 
				{
					System.out.println(i);
				}
			}
			
				
	}

}
