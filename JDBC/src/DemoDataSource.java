import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import oracle.jdbc.pool.OracleDataSource;

/*https://www.journaldev.com/2509/java-datasource-jdbc-datasource-example*/
public class DemoDataSource {
public static void main(String[] args) {
	
	DataSource dataSource=null;
	try {
		dataSource=myDataSource();
		Connection conn=dataSource.getConnection();
		System.out.println("Connected");
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}

public static DataSource myDataSource() throws SQLException
{
	OracleDataSource datasource=new OracleDataSource();
	datasource.setURL("jdbc:oracle:thin:@vhkdld387:1551:staffd");
	datasource.setUser("hr");
	datasource.setPassword("Eagle#2019");
	return datasource;
}
}
