package com.method;

import java.lang.reflect.Method;
import java.util.function.BiFunction;

public class method_Reference {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		BiFunction<Integer,Integer,Integer> adder=method_Reference::add;
		BiFunction<Float,Integer,Float> adder1=method_Reference::add;
		BiFunction<Float,Float,Float> adder2=method_Reference::add;
		
		
		System.out.println(adder.apply(2, 3));
		System.out.println(adder1.apply(2f, 3));
		System.out.println(adder2.apply(2f, 3f));//static method
		
		System.out.println(calculate(adder2,4.0f,3.0f));
		//System.out.println(calculate(adder1,4f,3));
		
		method_Reference m=new method_Reference();
		BiFunction<Integer,Integer,Integer> adder3=m::add1;
		System.out.println(adder3.apply(2,3));//non-static method
	}
	public static float add(float a,float b)
	{
		return a+b;
	}
	public static float add(float a,int b)
	{
		return a+b;
	}
	public static int add(int a,int b)
	{
		return a+b;
	}
	public int add1(int a,int b)
	{
		return a+b;
	}
	public static float calculate(BiFunction<Float,	Float, Float> bifun,float a,float b)
	{
		return (float) bifun.apply(a,b);
	}
	
}
