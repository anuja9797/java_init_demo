
public class MyClass {
//function of class: data and processing
	
//data: Data Member
	private int value;
	private String name;
	
//process: Module/Member function
	
	public void display()
	{
		System.out.println(value+"\t"+name);
	}

	public MyClass()
	{
		System.out.println("DEFAULT CONSTRUCTOR");
		value=1;
		name="anuja";
	}
	public MyClass(int value, String name) {
		System.out.println("PARAMETERIZED CONSTRUCTOR");
		this.value = value;
		this.name = name;
	}
	
	public void setValue(int value)
	{
		this.value=value;
	}
	public int getValue()
	{
		return value;
	}
	public void setName(String name)
	{
		this.name=name;
	}
	public String getName()
	{
		return name;
	}

}
