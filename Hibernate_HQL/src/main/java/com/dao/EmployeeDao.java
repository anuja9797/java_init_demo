package com.dao;



import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.query.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;

import com.beans.Employee;
import com.util.HibernateUtil;

public class EmployeeDao {

	//ADD EMPLOYEE
	public Integer addEmployee(String fname,String lname,int salary)
	{
		Integer empId=null;
		try(Session session =HibernateUtil.getSessionKey().openSession();)
		{
			Transaction tx=null;
			tx=session.beginTransaction();
			Employee emp=new Employee(fname,lname,salary);
			empId=(Integer)session.save(emp);
			tx.commit();
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}finally {
			HibernateUtil.shutdown();
		}
		
		return empId;
	}
	
	//FIND ALL EMP
	public List listEmployee()
	{
		Transaction tx=null;
		List emp=null;
		try(Session session =HibernateUtil.getSessionKey().openSession();)
		{
			
			tx=session.beginTransaction();
			CriteriaBuilder builder=session.getCriteriaBuilder();
			CriteriaQuery<Employee> cquery=builder.createQuery(Employee.class);
			Root<Employee> root=cquery.from(Employee.class);
			cquery.select(root);
			emp=session.createQuery(cquery).getResultList();
			tx.commit();
			
		}catch(Exception e)
		{
			if(tx!=null)
				tx.rollback();
			e.printStackTrace();
		}finally {
			HibernateUtil.shutdown();
		}
		
		return emp;
	}

	//FIND ALL EMP with SALARY less than sal
	public List listEmployeeByLTSalary(int sal)
	{
		Transaction tx=null;
		List emp=null;
		try(Session session =HibernateUtil.getSessionKey().openSession();)
		{
			
			tx=session.beginTransaction();
			CriteriaBuilder builder=session.getCriteriaBuilder();
			CriteriaQuery<Employee> cquery=builder.createQuery(Employee.class);
			Root<Employee> root=cquery.from(Employee.class);
			cquery.select(root).where(builder.lt(root.get("salary"),sal));
			emp=session.createQuery(cquery).getResultList();
			tx.commit();
			
		}catch(Exception e)
		{
			if(tx!=null)
				tx.rollback();
			e.printStackTrace();
		}finally {
			HibernateUtil.shutdown();
		}
		
		return emp;
	}
	
	//FIND ALL EMP with SALARY greater than equal to sal
		public List listEmployeeByGESalary(int sal)
		{
			Transaction tx=null;
			List emp=null;
			try(Session session =HibernateUtil.getSessionKey().openSession();)
			{
				
				tx=session.beginTransaction();
				CriteriaBuilder builder=session.getCriteriaBuilder();
				CriteriaQuery<Employee> cquery=builder.createQuery(Employee.class);
				Root<Employee> root=cquery.from(Employee.class);
				cquery.select(root).where(builder.ge(root.get("salary"),sal));
				emp=session.createQuery(cquery).getResultList();
				tx.commit();
				
			}catch(Exception e)
			{
				if(tx!=null)
					tx.rollback();
				e.printStackTrace();
			}finally {
				HibernateUtil.shutdown();
			}
			
			return emp;
		}
		
		//FIND ALL EMP with SALARY equal to sal HQL
				public List findByCol(int sal)
				{
					Transaction tx=null;
					List<Object[]> list=null;
					try(Session session =HibernateUtil.getSessionKey().openSession();)
					{
						
						tx=session.beginTransaction();
						String HQL_QUERY="select "+
										"e.id as empId, e.firstname as empName "+
										"from com.beans.Employee e "+
										"where e.salary=:sal";
						Query<Object[]> query=session.createQuery(HQL_QUERY);
						query.setParameter("sal", sal);
						list=query.getResultList();
						tx.commit();
						
					}catch(Exception e)
					{
						e.printStackTrace();
						if(tx!=null)
							tx.rollback();
						
					}finally {
						HibernateUtil.shutdown();
					}
					
					return list;
				}
				//FIND ALL EMP with SALARY equal to sal HQL 2nd WAY
				public List<Map<String,Object>> findByCol_Map(int sal)
				{
					Transaction tx=null;
					List<Map<String,Object>> list=null;
					try(Session session =HibernateUtil.getSessionKey().openSession();)
					{
						
						tx=session.beginTransaction();
						String HQL_QUERY="select "+
										"e.id as empId, e.firstname as empName "+
										"from com.beans.Employee e "+
										"where e.salary=:sal";
						Query<Map<String,Object>> query=session.createQuery(HQL_QUERY);
						query.setParameter("sal", sal);
//						list=query.getResultList();
						query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP); //
						list=query.list();//
						tx.commit();
						
					}catch(Exception e)
					{
						e.printStackTrace();
						if(tx!=null)
							tx.rollback();
						
					}finally {
						HibernateUtil.shutdown();
					}
					
					return list;
				}
				public void countCol()
				{
					Transaction tx=null;
					List<Map<String,Object>> list=null;
					try(Session session =HibernateUtil.getSessionKey().openSession();)
					{
						
						tx=session.beginTransaction();
						String HQL_Query="select count(1) from com.beans.Employee"; //count(1) means number of columns to be obtained
						List<Long> totalDept=session.createQuery(HQL_Query,Long.class).getResultList();

						System.out.println("Total employees"+totalDept.get(0));
						Criteria cr=session.createCriteria(Employee.class);//It gives deprecation but code works
						cr.setProjection(Projections.rowCount());
						List rowcount=cr.list();
						System.out.println("Thrg Criteria"+rowcount.get(0));
						cr.setProjection(Projections.sum("salary"));
						List Total=cr.list();
						System.out.println(Total.get(0));
						tx.commit();
						
					}catch(Exception e)
					{
						e.printStackTrace();
						if(tx!=null)
							tx.rollback();
						
					}finally {
						HibernateUtil.shutdown();
					}
					
					
				}
				
				public void updateEmployee(int salary,int id)
				{
					Transaction tx=null;
					
					try(Session session =HibernateUtil.getSessionKey().openSession();)
					{
						
						tx=session.beginTransaction();
						String HQL_Query="UPDATE com.beans.Employee set salary=:salary where id = :employee_id"; //count(1) means number of columns to be obtained
						Query query=session.createQuery(HQL_Query);
						query.setParameter("salary", salary);
						query.setParameter("employee_id", id);
						int result=query.executeUpdate();
						
						System.out.println("ROWS AFFECTED "+result);
						tx.commit();
						
					}catch(Exception e)
					{
						System.out.println("error");
						e.printStackTrace();
						if(tx!=null)
							tx.rollback();
						
					}finally {
						HibernateUtil.shutdown();
					}
					
					
				}
				public void hib_Query()
				{
					Transaction tx=null;
					
					try(Session session =HibernateUtil.getSessionKey().openSession();)
					{
						//Query query=session.createQuery("SELECT SUM(SALARY) FROM com.beans.Employee");
						Query query=session.createQuery("select sum(salary) from com.beans.Employee");
						System.out.println("SUM(SALARY) "+query.list().get(0));

						Query query1=session.createQuery("select MAX(salary) from com.beans.Employee");
						System.out.println("max(SALARY) "+query.list().get(0));
						
						Query query2=session.createQuery("select min(salary) from com.beans.Employee");
						System.out.println("min of salary "+query2.list().get(0));


						Query query3=session.createQuery("select avg(salary) from com.beans.Employee");
						System.out.println("avg of salary "+query3.list().get(0));
					}catch(Exception e)
					{
						System.out.println("error");
						e.printStackTrace();
						if(tx!=null)
							tx.rollback();
						
					}finally {
						HibernateUtil.shutdown();
					}
					
					
				}
				public void Query()
				{
					Transaction tx=null;
					List<Employee> list=new ArrayList<Employee>();
					try(Session session =HibernateUtil.getSessionKey().openSession();)
					{
						Query<Employee> qry=session.getNamedQuery("get_emp");
						list=qry.getResultList();
						System.out.println("SIZE OF LIST: "+list.size());
						for(Employee emp:list)
							System.out.println(emp);
						
						
						
					}catch(Exception e)
					{
						System.out.println("error");
						e.printStackTrace();
						if(tx!=null)
							tx.rollback();
						
					}finally {
						HibernateUtil.shutdown();
					}
					
					
				}
				public void Query(int sal)
				{
					
					List<Object[]> list=new ArrayList<Object[]>();
					try(Session session =HibernateUtil.getSessionKey().openSession();)
					{
						Query<Object[]> qry1=session.getNamedQuery("get_bysal");
						qry1.setParameter("sal", sal);
						
						list=qry1.getResultList();
						System.out.println("SIZE OF LIST: "+list.size());
						list.forEach((o)->{
							System.out.println("Id: "+o[0]+" Name: "+o[1]  );
							});
						
					}catch(Exception e)
					{
						System.out.println("error");
						e.printStackTrace();
						
					}
					
					
				}

				@SuppressWarnings("unchecked")
				public List<Object[]> findAllEmp_NativeSQL()
				{
					Transaction tx=null;
					List<Object[]> list=new ArrayList<Object[]>();
					try(Session session =HibernateUtil.getSessionKey().openSession();)
					{
						
						tx=session.beginTransaction();
						list=session.createNativeQuery("select * from anu_emplooyee_HQL")
								.addScalar("id",IntegerType.INSTANCE)
								.addScalar("firstname",StringType.INSTANCE)
								.addScalar("lastname",StringType.INSTANCE)
								.addScalar("salary",IntegerType.INSTANCE)
								.list();
								
						for(Object[] objs:list)
						{
							Integer id=(Integer) objs[0];
							String fname=(String) objs[1];
							String lname=(String) objs[2];
							Integer sal=(Integer) objs[3];
							System.out.println("ID: "+id+"\tName: "+fname+" "+lname+"\tsal: "+sal);
						}
						tx.commit();
					
					}catch(Exception e)
					{
						System.out.println("error");
						e.printStackTrace();
						if(tx!=null)
							tx.rollback();
						
					}finally {
						HibernateUtil.shutdown();
					}
					return list;
					
				}
}
