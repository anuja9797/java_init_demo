package com.beans;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Demo {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int demoId;
	private String name;

	public Demo() {
		// TODO Auto-generated constructor stub
	}

	public Demo(int demoId, String name) {
		super();
		this.demoId = demoId;
		this.name = name;
	}
	
	public Demo( String name) {
		super();
		this.name = name;
	}


	public int getDemoId() {
		return demoId;
	}

	public void setDemoId(int demoId) {
		this.demoId = demoId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
