package com.decorator;

public abstract class Sandwich {
protected String description ="Sandwich";
public abstract Long price();
public String getDescription() {
	return description;
}

}
