package com.pack2;

import com.pack1.Test1;
import static com.pack1.Test1.show;
public class Test2 {

	void myTest2()
	{
		System.out.println("INVOKING MYTEST2");
		Test1 t=new Test1();
		t.myTest1(); //STATIC between packages(outside pack) using IMPORT => public
		
		
		//Test1.show("anuja");  //NONSTATIC outside pack =>public
		show("static import");  //NONSTATIC outside pack =>public
	}
}
