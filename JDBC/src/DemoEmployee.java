import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import com.pojo.Employee;

public class DemoEmployee {

	public Connection conn;
	private Connection myConnection()
	{
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			System.out.println("DRIVER LOADED");	
			conn = DriverManager.getConnection("jdbc:oracle:thin:@vhkdld387:1551:staffd","hr","Eagle#2019");
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} //url, username, pwd
		return conn;
	}
    public DemoEmployee() throws ClassNotFoundException, SQLException {
	// TODO Auto-generated constructor stub
	
	conn=null;
	conn=myConnection();
}	
    
    
int addEmployee(Employee emp)
{
	
	
	System.out.println("Connected");
	try {
		String INSERT_TABLE=" insert into anu_employee values(?,?,?,?,?,?)";

		PreparedStatement ps;
		ps = conn.prepareStatement(INSERT_TABLE);
		ps.setInt(1,emp.getEmpId());
		ps.setString(2,emp.getEmpName());
		ps.setString(3,emp.getEmpAddress());
		ps.setInt(4,emp.getEmpAge());
		ps.setDouble(5,emp.getEmpExperience());
		ps.setString(6,emp.getDept());
		int ret=ps.executeUpdate();
		return ret;
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	return 0;
}

public boolean updateEmployee(int empid,Employee emp)
{
	
	
	System.out.println("Connected");
	try {
		String UPDATE_TABLE="UPDATE anu_employee SET empName=?,empAddress=?,empAge=?,empExperience=?,dept=? WHERE empId=?";

		PreparedStatement ps;
		ps = conn.prepareStatement(UPDATE_TABLE);
		
		ps.setString(1,emp.getEmpName());
		ps.setString(2,emp.getEmpAddress());
		ps.setInt(3,emp.getEmpAge());
		ps.setDouble(4,emp.getEmpExperience());
		ps.setString(5,emp.getDept());
		ps.setInt(6,emp.getEmpId());
		int ret=ps.executeUpdate();
		if(ret!=0)
			{
			//System.out.println("Updated "+ret+" rows");
			return true;
			}
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	return false;
}

public Employee getEmployeeById(int empId)
{
	Employee emp=new Employee();
	
	try {
		String FIND_ALL=" SELECT * FROM ANU_Employee  WHERE empId=?";
		PreparedStatement ps;
		ps = conn.prepareStatement(FIND_ALL);
		ps.setInt(1,empId);
		ResultSet rs=ps.executeQuery();
		while(rs.next())
		{
			int empid=rs.getInt(1);
			String name=rs.getString(2);
			String address=rs.getString(3);
			int age=rs.getInt(4);
			int exp=rs.getInt(5);
			String dept=rs.getString(6);
			emp=new Employee(empid,name,address,age,exp,dept);
		//	System.out.println(emp);
			return emp;
		}
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	return emp;
}

public List<Employee> getAllEmployee()
{
	List<Employee> list=new ArrayList<Employee>();
	
	Employee emp=null;
	
	try {
		String FIND_ALL=" SELECT * FROM ANU_Employee";
		PreparedStatement ps;
		ps = conn.prepareStatement(FIND_ALL);
		ResultSet rs=ps.executeQuery();
		while(rs.next())
		{
			int empid=rs.getInt(1);
			String name=rs.getString(2);
			String address=rs.getString(3);
			int age=rs.getInt(4);
			int exp=rs.getInt(5);
			String dept=rs.getString(6);
			emp=new Employee(empid,name,address,age,exp,dept);
			list.add(emp);
		}
		//return list;
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	return list;
}

public List<Employee> getAllEmployeeByExp(int exp1)
{
	List<Employee> list=new ArrayList<Employee>();
	
	Employee emp=null;
	
	try {
		String FIND_ALL=" SELECT * FROM ANU_Employee WHERE empExperience =?";
		PreparedStatement ps;
		ps = conn.prepareStatement(FIND_ALL);
		ps.setInt(1,exp1);
		ResultSet rs=ps.executeQuery();
		while(rs.next())
		{
			int empid=rs.getInt(1);
			String name=rs.getString(2);
			String address=rs.getString(3);
			int age=rs.getInt(4);
			int exp=rs.getInt(5);
			String dept=rs.getString(6);
			emp=new Employee(empid,name,address,age,exp,dept);
			list.add(emp);
		}
		//return list;
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	return list;
}

public List<Employee> getAllEmployeeByDept(String dept1)
{
	List<Employee> list=new ArrayList<Employee>();
	
	Employee emp=null;
	
	try {
		String FIND_ALL=" SELECT * FROM ANU_Employee WHERE dept =?";
		PreparedStatement ps;
		ps = conn.prepareStatement(FIND_ALL);
		ps.setString(1,dept1);
		ResultSet rs=ps.executeQuery();
		while(rs.next())
		{
			int empid=rs.getInt(1);
			String name=rs.getString(2);
			String address=rs.getString(3);
			int age=rs.getInt(4);
			int exp=rs.getInt(5);
			String dept=rs.getString(6);
			emp=new Employee(empid,name,address,age,exp,dept);
			list.add(emp);
		}
		return list;
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	return list;
}
}
