package com.collection;

import java.util.Properties;

public class Country {
private String name;
private String continent;
private Properties state_capitals;

public Country() {
	// TODO Auto-generated constructor stub
}

public void printCapitals()
{
	for(String state:state_capitals.stringPropertyNames())
	{
		System.out.println("State: "+state+"\tCapital: "+state_capitals.getProperty(state));
	}
}
@Override
public String toString() {
	String res="Country [name=" + name + ", continent=" + continent + ", state_capitals=[\n";
	for(String state:state_capitals.stringPropertyNames())
	{
		res+="State: "+state+"\tCapital: "+state_capitals.getProperty(state)+"\n";
	}
	res+="]]";
	return res;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public String getContinent() {
	return continent;
}

public void setContinent(String continent) {
	this.continent = continent;
}

public Properties getState_capitals() {
	return state_capitals;
}

public void setState_capitals(Properties state_capitals) {
	this.state_capitals = state_capitals;
}



}
