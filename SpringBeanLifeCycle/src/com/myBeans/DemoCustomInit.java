package com.myBeans;

public class DemoCustomInit {
private String message;
private String name;

public DemoCustomInit() {
	// TODO Auto-generated constructor stub
	System.out.println("DEFAULT CONSTRUCTOR");
	message="Welcome!";
	name="CLSA";
			
}

@Override
public String toString() {
	return message+"\t"+name;
}

public String getMessage() {
	return message;
}

public void setMessage(String message) {
	this.message = message;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}
public void myInit()
{
	name=name.toUpperCase();
	message=message.toUpperCase();
	System.out.println("myInit() METHOD CALLED");
}

public void myDestroy()
{
	name=null;
	message=null;			
	System.out.println("myDestroy() METHOD CALLED");
}
}
