package com.pojo;

public class Book {

	private String bookName;
	private long ISBN;
	private String publication,description,author;
	private int price;
	
	public Book() {
		// TODO Auto-generated constructor stub
//		this.bookName="Rainwater";
//		this.ISBN=123456789;
//		this.publication="BROWN PUB";
//		this.price=250;
//		this.description="GOOD NOVEL";
//		this.author="S BROWN";
	}
	
	
	public Book(String bookName, long iSBN, String publication, String description, String author, int price) {
		super();
		this.bookName = bookName;
		ISBN = iSBN;
		this.publication = publication;
		this.description = description;
		this.author = author;
		this.price = price;
	}


	@Override
	public String toString() {
		return "Book [bookName=" + bookName + ", ISBN=" + ISBN + ", publication=" + publication + ", description="
				+ description + ", author=" + author + ", price=" + price + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Book other = (Book) obj;
		if (ISBN != other.ISBN)
			return false;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (bookName == null) {
			if (other.bookName != null)
				return false;
		} else if (!bookName.equals(other.bookName))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (price != other.price)
			return false;
		if (publication == null) {
			if (other.publication != null)
				return false;
		} else if (!publication.equals(other.publication))
			return false;
		return true;
	}


	public String getBookName() {
		return bookName;
	}
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
	public long getISBN() {
		return ISBN;
	}
	public void setISBN(long iSBN) {
		ISBN = iSBN;
	}
	public String getPublication() {
		return publication;
	}
	public void setPublication(String publication) {
		this.publication = publication;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	
}
/*
 SQL> desc anu_books;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 BOOKNAME                                           VARCHAR2(20)
 ISBN                                      NOT NULL NUMBER
 PUBLICATION                                        VARCHAR2(20)
 DESCRIPTION                                        VARCHAR2(20)
 AUTHOR                                             VARCHAR2(20)
 PRICE                                              NUMBER(38)

SQL> create table anu_books(bookName varchar(20),ISBN number not null,publication varchar(20),description varchar(20),author varchar(20),price int,primary key(ISBN));
*/
 