package com.decorator;

public class SandwichMaker {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Sandwich mysandwich=new WheatBreadSandwich("WHEAT BREAD SANDWICH");
		System.out.println("Price :"+mysandwich.price()+" with "+mysandwich.getDescription());
		
		mysandwich=new CheeseDecorator(mysandwich);
		System.out.println("Price :"+mysandwich.price()+" with "+mysandwich.getDescription());
		
	}

}
