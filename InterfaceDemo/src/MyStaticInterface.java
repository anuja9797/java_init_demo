
public interface MyStaticInterface {

	void add(int x,int y);
	default void showMessage(String name)
	{
		System.out.println("WELCOME "+name);
	}
	
	static void cal(int a,int b)
	{
		System.out.println("Interface 1 ADDITION IS:"+(a+b));
	}
}
