package com.pojo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class Employee {
@Value(value="10")
private int empId;
@Value(value="Anuja")
private String empName;
@Autowired
@Qualifier(value="address1")        //By default is ByType hence multiple beans with same type creates exception
private Address address;
//@Bean
public int getEmpId() {
	return empId;
}
public void setEmpId(int empId) {
	this.empId = empId;
}
public String getEmpName() {
	return empName;
}
public void setEmpName(String empName) {
	this.empName = empName;
}
public Address getAddress() {
	return address;
}
public void setAddress(Address address) {
	this.address = address;
}
@Override
public String toString() {
	return "Employee [empId=" + empId + ", empName=" + empName + ", address=" + address + "]";
}
public Employee() {
	// TODO Auto-generated constructor stub
}
public Employee(int empId, String empName, Address address) {
	super();
	this.empId = empId;
	this.empName = empName;
	this.address = address;
}

}
