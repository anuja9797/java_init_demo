import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class DemoMapModification {
public static void main(String[] args) {
	
	//Map<String, String> mymap=new HashMap<String,String>();
	Map<String, String> mymap=new ConcurrentHashMap<String,String>();
	mymap.put("anuja", "watpade");
	mymap.put("nikita", "kokitkar");
	mymap.put("juilee","joshi");
	
	Iterator<String> itr=mymap.keySet().iterator();
	while(itr.hasNext())
	{
		String key=itr.next();
		System.out.println("MAP key "+mymap.get(key));
		if(key.equals("anuja"))
		{
			mymap.put("anuja","ANUJA");
			mymap.put("nikita1","ANUJA");
		}
	}
	System.out.println("------");
	 itr=mymap.keySet().iterator();
	while(itr.hasNext())
	{
		String key=itr.next();
		System.out.println("MAP key "+key+" VAL "+mymap.get(key)+" ");
		
	}
	
}
}
