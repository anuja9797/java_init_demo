package com.collection;

import java.util.Set;

public class MyAddressSet {
	Set<Address> addresses;
	public MyAddressSet() {
		// TODO Auto-generated constructor stub
	}
	public Set<Address> getAddresses() {
		return addresses;
	}
	public void setAddresses(Set<Address> addresses) {
		this.addresses = addresses;
	}
	@Override
	public String toString() {
		String res="";
		for(Address a:addresses)
		{
			res+=(a.toString()+"\n");
		}
		return res;
		//return "MyAddressSet [addresses=" + addresses + "]";
	}
}
