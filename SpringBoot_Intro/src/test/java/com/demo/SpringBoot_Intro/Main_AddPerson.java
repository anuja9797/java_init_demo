package com.demo.SpringBoot_Intro;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Assert;
import org.springframework.web.client.RestTemplate;

import com.beans.Person;

public class Main_AddPerson {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		RestTemplate template=new RestTemplate();
		SimpleDateFormat dateformat =new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
		Date newdate=null;
		String strdate="02-04-2018 11:35:42";
		try {
			newdate = dateformat.parse(strdate);
			System.out.println(newdate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//		Person person=new Person("person_test",123456,newdate);
//		Person person_inserted = template.postForObject("http://localhost:8083/persons-data", person, Person.class);
//		System.out.println(person_inserted+" ADDED");
		
		Person person=new Person();
		person.setName("person_test2");
		person.setBirthDate(newdate);
		Person person_inserted = template.postForObject("http://localhost:8083/persons-data", person, Person.class);
		System.out.println(person_inserted+" ADDED");
		
	    Assert.assertEquals("person_test2", person.getName());
	}

}
