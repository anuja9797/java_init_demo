import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DemoJDBC {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Properties prop=new Properties();
		try(InputStream inp=DemoJDBC.class.getClassLoader().getResourceAsStream("jdbc.properties");)
		{
			prop.load(inp);
			String driver=prop.getProperty("db.driver.name");
			Class.forName(driver);
			System.out.println("Driver found!");
			
			Connection conn=DriverManager.getConnection(prop.getProperty("db.url"),prop.getProperty("db.username"),prop.getProperty("db.password"));
			System.out.println("Connected!!");
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
