package com.singleton;


public class Person_Eager {

	private String name;
	private static int rollno;
	private static final Person_Eager PERSON=new Person_Eager();
	private Person_Eager()
	{
		name="Eager";rollno=10;
	}
	
	public static Person_Eager getPersoninstance()
	{
		return PERSON;
	}
	public static void DisplayRoll()
	{
		System.out.println("Person Rollno: "+rollno);
	}
	
	@Override
	public String toString() {
		return "Person_Eager [name=" + name + "]";
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Person_Eager p1=new Person_Eager();//allowed
		Person_Eager p=Person_Eager.getPersoninstance();
		System.out.println(p);
		Person_Eager.DisplayRoll();
	}

}

//class Out
//{
//	void test() {
//
//		Person_Eager p1=new Person_Eager();
//		Person_Eager p=Person_Eager.getPersoninstance();
//		System.out.println(p);
//		Person_Eager.DisplayRoll();
//	}
//}