package com.beans;

public class MyData implements Comparable<MyData> {

	
	private int dataId;
	private String dataValue;
	private long data;
	public MyData() {
		// TODO Auto-generated constructor stub
	}
	
	public MyData(int dataId, String dataValue, long data) {
		super();
		this.dataId = dataId;
		this.dataValue = dataValue;
		this.data = data;
	}

	@Override
	public String toString() {
		return "MyData [dataId=" + dataId + ", dataValue=" + dataValue + ", data=" + data + "]";
	}

	@Override
	public boolean equals(Object a) {
		// TODO Auto-generated method stub
		MyData m=(MyData)a;
		return m.getDataId()==this.dataId && m.getDataValue().equals(this.dataValue)&&m.getData()==this.data;
	}

	public int getDataId() {
		return dataId;
	}

	public void setDataId(int dataId) {
		this.dataId = dataId;
	}

	public String getDataValue() {
		return dataValue;
	}

	public void setDataValue(String dataValue) {
		this.dataValue = dataValue;
	}

	public long getData() {
		return data;
	}

	public void setData(long data) {
		this.data = data;
	}
	public int hashCode()
	{
		return 0;//add logic
		
		/*
		 If two objects are equal according to the equals(Object) method, then calling the hashCode()
		 method on each of the two objects must produce the same value.
		 With same hashcode every object would be stored in the same, single bucket. 
		  
		 */
	}

	

	@Override
	public int compareTo(MyData arg0) {
		// TODO Auto-generated method stub
		
		//Default Comparator 
		
		//return arg0.getDataId()-this.getDataId();  //desc order wrt id
		//return this.getDataId()-arg0.getDataId(); //asc order
		return arg0.getDataValue().compareTo(this.getDataValue()); //desc order wrt str datavalue
	}


}
