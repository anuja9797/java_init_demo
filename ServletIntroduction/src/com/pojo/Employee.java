package com.pojo;

public class Employee {
@Override
	public boolean equals(Object arg0) {
		// TODO Auto-generated method stub
	    Employee emp=(Employee)arg0;
		return this.empId==emp.getEmpId() && this.empName.equals(emp.getEmpName());
	}

private int empId;
private String empName;
private String empAddress;
private int empAge;
private int empExperience;
private String dept;
public Employee() {
	// TODO Auto-generated constructor stub
	empId=0;
}
public Employee(int empId, String empName, String empAddress, int empAge, int empExperience, String dept) {
	super();
	this.empId = empId;
	this.empName = empName;
	this.empAddress = empAddress;
	this.empAge = empAge;
	this.empExperience = empExperience;
	this.dept = dept;
}
public int getEmpId() {
	return empId;
}
public void setEmpId(int empId) {
	this.empId = empId;
}
public String getEmpName() {
	return empName;
}
public void setEmpName(String empName) {
	this.empName = empName;
}
public String getEmpAddress() {
	return empAddress;
}
public void setEmpAddress(String empAddress) {
	this.empAddress = empAddress;
}
public int getEmpAge() {
	return empAge;
}
public void setEmpAge(int empAge) {
	this.empAge = empAge;
}
public int getEmpExperience() {
	return  empExperience;
}
public void setEmpExperience(int empExperience) {
	this.empExperience = empExperience;
}
public String getDept() {
	return dept;
}
public void setDept(String dept) {
	this.dept = dept;
}

@Override
public String toString() {
	return "Employee [empId=" + empId + ", empName=" + empName + ", empAddress=" + empAddress + ", empAge=" + empAge
			+ ", empExperience=" + empExperience + ", dept=" + dept + "]";
}

}
