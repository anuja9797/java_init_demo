package com.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Repository;

import com.beans.Person;

@Repository
(value="anu_hib_impl")
public class PersonDaoImpl implements PersonDAOInterface {
		@Autowired
		private EntityManager entityManager;
		
		
		@Override
		@Transactional
		public int addPerson(Person Person) {
			// TODO Auto-generated method stub
			System.out.println("add person");
			entityManager.persist(Person);
			return 1;
		}
		
		@Override
		public Person getPerson(int id) {
			// TODO Auto-generated method stub
			
			return entityManager.find(Person.class, id);
		
		}
		
		@Override
		public Person deletePerson(int id) {
		
			// TODO Auto-generated method stub
			return null;
		
		}
		
		@SuppressWarnings("unchecked")
		@Override
		public List<Person> getAllPersons_1() {
		
			// TODO Auto-generated method stub
			System.out.println("view all person");
			String hql="from com.beans.Person";
			return (List<Person>) entityManager.createQuery(hql).getResultList();
		
		}

}