package com.inheritance;

public class Child extends Parent {

	private int data_child;

	public Child() {
		// TODO Auto-generated constructor stub
	}
	


	public Child(int id, String data,int data_child) {
		super(id, data);
		this.data_child=data_child;
	}



	public int getData_child() {
		return data_child;
	}

	public void setData_child(int data_child) {
		this.data_child = data_child;
	}

	@Override
	public String toString() {
//		return "Child [data_child=" + data_child + "]"+"Parent [id=" + id + ", data=" + data + "]";
		return  "Child [data_child=" + data_child + "]"+super.toString();
	}
	
	
}
