package com.beans.association;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="Anu_PersonOnetoOne")

public class Person_One_to_One {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int personId;
	private String name;
	
	@OneToOne(fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	Address_One_to_One address;
	
	public Person_One_to_One() {
		// TODO Auto-generated constructor stub
		super();
	}

	public int getPersonId() {
		return personId;
	}

	public void setPersonId(int personId) {
		this.personId = personId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Address_One_to_One getAddress() {
		return address;
	}

	public void setAddress(Address_One_to_One address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Person_One_to_One [personId=" + personId + ", name=" + name + ", address=" + address + "]";
	}
	
	
}
