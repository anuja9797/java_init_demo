package com.demo.SpringBoot_Intro;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

//Nothing should be added in this class

@SpringBootApplication
@ComponentScan(basePackages="com") //To discover Annotaions in whole project ,always put this
@EnableJpaRepositories(basePackages= { "com.dao" })
@EntityScan("com.beans")//to discover entitiies

public class SpringBootIntroApplication {
	public static void main(String[] args) {
	
		SpringApplication.run(SpringBootIntroApplication.class, args);
	
	}


}