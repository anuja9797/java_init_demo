<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="com.pojo.Employee"%>
    <%@page import="java.util.List" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%
List<Employee> list=(List<Employee>)request.getAttribute("emplist");
%>

<br>
<table border="2">
	<% for(Employee e:list){ %>
	<tr>
	<td> <%out.println(e.getEmpId()); %></td>
	<td> <%out.println(e.getEmpName()); %></td>
	<td> <%out.println(e.getEmpAddress()); %></td>
	<td> <%=e.getDept() %></td>
	</tr>
	<% } %>
</table>
</body>
</html>