package com.beans.association.joinColumn;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Anu_PerOtoM_Col")

public class Person_OneToMany_Col {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int personId;
	private String name;
	
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="person_id")
	List<Address_OnrToMany_Col> addresses;
	
	public Person_OneToMany_Col() {
		// TODO Auto-generated constructor stub
		super();
	}

	public int getPersonId() {
		return personId;
	}

	public void setPersonId(int personId) {
		this.personId = personId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public List<Address_OnrToMany_Col> getAddress() {
		return addresses;
	}

	public void setAddress(List<Address_OnrToMany_Col> addresses) {
		this.addresses = addresses;
	}

	@Override
	public String toString() {
		return "Person_One_to_One [personId=" + personId + ", name=" + name + ", addresses list=" + addresses + "]";
	}
	
	
}
