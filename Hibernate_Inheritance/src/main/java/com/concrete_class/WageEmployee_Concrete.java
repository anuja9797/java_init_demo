package com.concrete_class;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="Anu_WageEmployeeConcrete")
public class WageEmployee_Concrete extends Employee_Concrete{

	private int wages,hours;
	
	public WageEmployee_Concrete() {
		// TODO Auto-generated constructor stub
	}

	public WageEmployee_Concrete(int wages, int hours, String name, String address) {
		super (name, address);
		this.wages = wages;
		this.hours = hours;
		// TODO Auto-generated constructor stub
	}
	public int getWages() {
		return wages;
	}

	public void setWages(int wages) {
		this.wages = wages;
	}

	public int getHours() {
		return hours;
	}

	public void setHours(int hours) {
		this.hours = hours;
	}
	
}
