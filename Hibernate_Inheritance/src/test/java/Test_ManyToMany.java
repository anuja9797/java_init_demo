import java.util.HashSet;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.many_to_many.Category;
import com.many_to_many.Stock;



public class Test_ManyToMany {

	public Test_ManyToMany() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Configuration configuration = new Configuration().configure();
		try(
			SessionFactory sessionFactory = configuration.buildSessionFactory();
			Session session = sessionFactory.openSession();)
		{
		
		session.beginTransaction();
		Stock s=new Stock();
		s.setStockCode("101");
		s.setStockname("STOCK101");
		
		Category c1=new Category("c1","category one");
		Category c2=new Category("c3","category three");
		Set<Category> cats=new HashSet<>();
		cats.add(c1);
		cats.add(c2);
		
		s.setCategories(cats);
		
		session.save(s);
		session.getTransaction().commit();
		
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}
/*
create table Anu_Category (
CATEGORY_ID number(10,0) not null,
 description varchar2(255 char),
 NAME varchar2(10 char) not null,
 primary key (CATEGORY_ID)
)


create table Anu_Stock (
STOCK_ID number(10,0) not null,
 STOCK_CODE varchar2(10 char) not null,
 STOCK_NAME varchar2(20 char) not null,
 primary key (STOCK_ID)
)


create table anu_stock_category (
STOCK_ID number(10,0) not null,
 CATEGORY_ID number(10,0) not null,
 primary key (STOCK_ID, CATEGORY_ID)
)


alter table Anu_Stock 
add constraint UK_8d98vy6wbcuc68qsoq9p9u6cr unique (STOCK_CODE)


alter table Anu_Stock 
add constraint UK_qf7myj2honmx66wy0brg6xsx2 unique (STOCK_NAME)


alter table anu_stock_category 
add constraint FKpp3npduxss501d27tcgjtdkwl 
foreign key (CATEGORY_ID) 
references Anu_Category


alter table anu_stock_category 
add constraint FKtrunn34shq0xgfhg66fwyy3cf 
foreign key (STOCK_ID) 
references Anu_Stock
*/