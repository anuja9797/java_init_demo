package com.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.beans.Person;

public class PersonMapper implements RowMapper<Person> {

@Override
public Person mapRow(ResultSet rs, int arg1) throws SQLException {
		// TODO Auto-generated method stub
		Person p=new Person();
		p.setPersonId(rs.getInt(1));
		p.setName(rs.getString(2));
		p.setBirthDate(rs.getDate(3));
		return p;

}

}