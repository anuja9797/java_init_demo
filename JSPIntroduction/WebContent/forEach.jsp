<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<%
	String[] numbers={"one","two","three","four","five"};
			pageContext.setAttribute("num", numbers);
	%>
	<table border="1">
	<tr>
	<td><b>myNum</b></td>
	<td><b>current</b></td>
	<td><b>first</b></td>
	<td><b>last</b></td>
	</tr>
	<c:forEach var="mynum" varStatus="st" items="${num }">
	<tr>
	<td><c:out value="${mynum}"></c:out></td>
	<td><c:out value="${st.current}"></c:out></td>
	<td><c:out value="${st.first}"></c:out></td>
	<td><c:out value="${st.last}"></c:out></td>
	</tr>
	</c:forEach>
	</table>
</body>
</html>