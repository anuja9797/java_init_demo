package com.annotation;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;

public class ProcessAnnotation {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Class<DemoAnnotation> demoClassObj=DemoAnnotation.class;
		try {
			Method method=demoClassObj.getMethod("display", new Class[] {});
			readAnnotation(method);
			
			Method method1=demoClassObj.getMethod("display", new Class[] {java.lang.String.class});
			readAnnotation(method1);
			
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void readAnnotation(AnnotatedElement element)
	{
		System.out.println("\n\nFINDING ANNOTATIONS ON: "+element.getAnnotations());
		Annotation[] anns=element.getAnnotations();
		for(Annotation a:anns)
		{
			if(a instanceof MyAnnotation) {
				MyAnnotation ann=(MyAnnotation)a;
				System.out.println("AUTHOR:"+ann.name());
				System.out.println("Version: "+ann.value());
			}
			
		}
	}
}
