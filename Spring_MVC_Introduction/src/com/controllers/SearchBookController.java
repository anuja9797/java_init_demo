package com.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.pojo.Book;

@Controller
public class SearchBookController {

	@RequestMapping(value="searchBooks.htm")
	public ModelAndView searchBookByAuthor(String author)
	{
		List<Book> books=new ArrayList<Book>();
		books.add(new Book("Hibernate",125698,"ABC","WELL WRITTEN",author,500));
		books.add(new Book("Hibernate 2.0",125697,"ABC","WELL WRITTEN",author,550));
		books.add(new Book("Spring 1.0",135697,"XYZ","GOOD","joshi",600));
		ModelAndView mav=new ModelAndView("display","book_list",books);
		mav.addObject("auth_name",author);
		return mav;
				
	}
	
	@RequestMapping(value="searchBooks1.htm",method= RequestMethod.GET)
	public ModelAndView searchBookByAuthor1(@RequestParam("author") String author_name)
	{
		List<Book> books=new ArrayList<Book>();
		books.add(new Book("Hibernate",125698,"ABC","WELL WRITTEN",author_name,500));
		books.add(new Book("Hibernate 2.0",125697,"ABC","WELL WRITTEN",author_name,550));
		books.add(new Book("Spring 1.0",135697,"XYZ","GOOD","joshi",600));
		ModelAndView mav=new ModelAndView("display","book_list",books);
		mav.addObject("auth_name",author_name);
		return mav;
				
	}
	
}
