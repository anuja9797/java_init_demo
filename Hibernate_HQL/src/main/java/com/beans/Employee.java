package com.beans;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

//@NamedQuery(name="get_emp", query="from com.beans.Employee") //SINGLE QUERY

@NamedQueries({
		@NamedQuery(name="get_emp", query="from com.beans.Employee"),
		@NamedQuery(name="get_bysal",query="select emp.id as empId, emp.firstname as empName from com.beans.Employee emp where emp.salary=:sal")
})
@Entity(name="anu_emplooyee_HQL")
public class Employee {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private int id;
	private String lastname;
	private String firstname;
	private int salary;
	public Employee() {
		// TODO Auto-generated constructor stub
	}
	public Employee(String firstname,String lastname,  int salary) {
		super();
		this.lastname = lastname;
		this.firstname = firstname;
		this.salary = salary;
	}
	@Override
	public String toString() {
		return "Employee [id=" + id + ", lastname=" + lastname + ", firstname=" + firstname + ", salary=" + salary
				+ "]";
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}

}
