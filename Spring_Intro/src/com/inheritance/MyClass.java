package com.inheritance;

public class MyClass {
private int id;
private String data;
private String m_data;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getData() {
	return data;
}
public void setData(String data) {
	this.data = data;
}
public String getM_data() {
	return m_data;
}
public void setM_data(String m_data) {
	this.m_data = m_data;
}
public MyClass(int id, String data, String m_data) {
	super();
	this.id = id;
	this.data = data;
	this.m_data = m_data;
}
public MyClass() {
	// TODO Auto-generated constructor stub
}
@Override
public String toString() {
	return "MyClass [id=" + id + ", data=" + data + ", m_data=" + m_data + "]";
}

}
