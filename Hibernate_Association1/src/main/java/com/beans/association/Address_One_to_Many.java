package com.beans.association;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Anu_AddOtoM")
public class Address_One_to_Many {
@Id
private int id;
private Long pincode;
private String city;

public Address_One_to_Many() {
	// TODO Auto-generated constructor stub
}

public Address_One_to_Many(int id, String city, Long pincode) {
	super();
	this.id = id;
	this.pincode = pincode;
	this.city = city;
}

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public Long getPincode() {
	return pincode;
}

public void setPincode(Long pincode) {
	this.pincode = pincode;
}

public String getCity() {
	return city;
}

public void setCity(String city) {
	this.city = city;
}

@Override
public String toString() {
	return "Address_One_to_Many [id=" + id + ", pincode=" + pincode + ", city=" + city + "]";
}


}
