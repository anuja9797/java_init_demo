package com.myBeans;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class CheckCustomInit {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ApplicationContext context=new ClassPathXmlApplicationContext("beans.xml");
		System.out.println("-----------------------------------");
		DemoCustomInit demo=(DemoCustomInit)context.getBean("d");
		System.out.println(demo);
		System.out.println("-----------------------------------");
		DemoInitializingBean demobean=(DemoInitializingBean)context.getBean("d1");
		System.out.println(demobean);
		System.out.println("-----------------------------------");
		DemoInitializingBean demobean2=(DemoInitializingBean)context.getBean("d2");
		System.out.println(demobean2);
		//DEFAULT CONST > AFTERPROPERTIESSET > INIT >
		
//		((AbstractApplicationContext)context).registerShutdownHook();//calls Destroy
	}

}
