package com.observer;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class MyFrame  extends JFrame{

	public MyFrame(String name){
		// TODO Auto-generated constructor stub
		super(name);
		setLayout(null);
		
		JLabel label=new JLabel("Name:");
		label.setBounds(50, 50, 50, 20);
		add(label);
		
		JTextField textfield=new JTextField(50);
		textfield.setBounds(100, 50, 100, 20);
		add(textfield);
		
		JButton button=new JButton("Enter");
		button.setBounds(100,100,100,20);
		add(button);
		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				textfield.setText("Entered");
			}
			
		});
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		JFrame frame =new MyFrame("MyFrame1");
		frame.setSize(500,500);
		frame.setVisible(true);
	}

}
