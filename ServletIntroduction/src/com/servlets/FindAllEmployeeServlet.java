package com.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.EmployeeDAO;
import com.pojo.Employee;

/**
 * Servlet implementation class FindAllEmployeeServlet
 */
@WebServlet("/showEmployees")
public class FindAllEmployeeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			EmployeeDAO dao=new EmployeeDAO();
			
			List<Employee> list=dao.getAllEmployee();
//			String msg="";
//			for(Employee e:list)
//			{
//				msg.concat(e+"\n");
//			}
//			request.setAttribute("msg",	msg);
			request.setAttribute("emplist",	list);
			
			RequestDispatcher rd=request.getRequestDispatcher("show_jstl.jsp");
//			RequestDispatcher rd=request.getRequestDispatcher("show.jsp");
			rd.forward(request, response);

		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
