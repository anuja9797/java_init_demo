package com.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.EmployeeDAO;
import com.pojo.Employee;

/**
 * Servlet implementation class AddEmployeeServlet
 */
@WebServlet("/add")
public class AddEmployeeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String ename=request.getParameter("empName");
		int id=Integer.parseInt(request.getParameter("empId"));
		int age=Integer.parseInt(request.getParameter("empAge"));
		String add=request.getParameter("empAddress");
		String dept=request.getParameter("empDepartment");
		int exp=Integer.parseInt(request.getParameter("empExperience"));
		
		System.out.println(ename+"\t"+id+"\t"+age+"\t"+add+"\t"+dept+"\t"+exp);
		
		Employee emp=new Employee(id,ename,add,age,exp,dept);
		try {
			EmployeeDAO dao=new EmployeeDAO();
			int rows=dao.addEmployee(emp);
			if(rows>0)
			{
				System.out.println("ADDED");
				RequestDispatcher rd=request.getRequestDispatcher("index.html");
				rd.forward(request, response);
			}
			else
			{
				String msg="SORRY /_('o')_/ FILL THE FORM AGAIN";
				request.setAttribute("msg", msg);
				RequestDispatcher rd=request.getRequestDispatcher("addEmployee.jsp");
				rd.forward(request, response);
			}
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
