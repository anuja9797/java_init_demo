package com.beans.association;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Anu_PerOtoM")

public class Person_One_to_Many {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int personId;
	private String name;
	
	@OneToMany(fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	List<Address_One_to_Many> addresses;
	
	public Person_One_to_Many() {
		// TODO Auto-generated constructor stub
		super();
	}

	public int getPersonId() {
		return personId;
	}

	public void setPersonId(int personId) {
		this.personId = personId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public List<Address_One_to_Many> getAddress() {
		return addresses;
	}

	public void setAddress(List<Address_One_to_Many> addresses) {
		this.addresses = addresses;
	}

	@Override
	public String toString() {
		return "Person_One_to_One [personId=" + personId + ", name=" + name + ", addresses list=" + addresses + "]";
	}
	
	
}
