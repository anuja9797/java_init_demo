package com.beans.association;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Anu_AddressOnetoOne")
public class Address_One_to_One {
@Id
@GeneratedValue(strategy=GenerationType.AUTO)
private int id;
private Long pincode;
private String city;

public Address_One_to_One() {
	// TODO Auto-generated constructor stub
}

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public Long getPincode() {
	return pincode;
}

public void setPincode(Long pincode) {
	this.pincode = pincode;
}

public String getCity() {
	return city;
}

public void setCity(String city) {
	this.city = city;
}

@Override
public String toString() {
	return "Address_One_to_One [id=" + id + ", pincode=" + pincode + ", city=" + city + "]";
}


}
