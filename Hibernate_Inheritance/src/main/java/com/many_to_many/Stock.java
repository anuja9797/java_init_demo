package com.many_to_many;


import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="Anu_Stock",uniqueConstraints= {
		@UniqueConstraint(columnNames="STOCK_NAME"),
		@UniqueConstraint(columnNames="STOCK_CODE")})
public class Stock {

	private Integer stockId;
	private String stockCode;
	private String stockname;
	private Set<Category> categories=new HashSet<Category>(0);
	
	public Stock() {
		// TODO Auto-generated constructor stub
	}

	public Stock(String stockCode, String stockname) {
		super();
		this.stockCode = stockCode;
		this.stockname = stockname;
	}

	public Stock(String stockCode, String stockname, Set<Category> categories) {
		super();
		this.stockCode = stockCode;
		this.stockname = stockname;
		this.categories = categories;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="STOCK_ID",unique=true,nullable=false)
	public Integer getStockId() {
		return stockId;
	}

	public void setStockId(Integer stockId) {
		this.stockId = stockId;
	}

	@Column(name="STOCK_CODE",unique=true,nullable=false,length=10)
	public String getStockCode() {
		return stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	@Column(name="STOCK_NAME",unique=true,nullable=false,length=20)
	public String getStockname() {
		return stockname;
	}

	public void setStockname(String stockname) {
		this.stockname = stockname;
	}

	//join=same table's col
	//inv join=col of mapped table
	@ManyToMany(fetch = FetchType.LAZY, cascade= CascadeType.ALL)
	@JoinTable(name="anu_stock_category", 
			joinColumns= {
			@JoinColumn(name="STOCK_ID",nullable=false,updatable=false)
			} ,
			inverseJoinColumns= {
				@JoinColumn(name="CATEGORY_ID",nullable=false,updatable=false)	
			}
	)
	public Set<Category> getCategories() {
		return categories;
	}

	public void setCategories(Set<Category> categories) {
		this.categories = categories;
	}

}
