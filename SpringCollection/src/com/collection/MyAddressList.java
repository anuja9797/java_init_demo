package com.collection;

import java.util.List;

public class MyAddressList {

	List<Address> addresses;

	public List<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}

	@Override
	public String toString() {
		String res="";
		for(Address a:addresses)
		{
			res+=(a.toString()+"\n");
		}
		return res;
		//return "MyAddressList [addresses=" + addresses + "]";
	}
	
}
