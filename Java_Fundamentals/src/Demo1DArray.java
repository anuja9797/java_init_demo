
public class Demo1DArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//declare array
		int nums[];
		//initialize array
		nums=new int[5];
		nums[0]=0;
//		nums[5]=5;  runtime exception
 		for(int i=1;i<nums.length;i++)
			nums[i]=i;
// 		System.out.println(nums);
		for(int i=0;i<nums.length;i++)
			System.out.println(nums[i]);
		int tot=0;
		for(int a : nums)
		{
			System.out.println(a);tot=tot+a;
		}
		System.out.println("Total:"+tot);
	}

}
