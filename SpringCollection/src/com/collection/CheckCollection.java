package com.collection;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class CheckCollection {
@SuppressWarnings("resource")
public static void main(String[] args) {
	ApplicationContext context=new ClassPathXmlApplicationContext("beans.xml");
	Address add=(Address)context.getBean("add");
	System.out.println(add);
	
	MyAddressList add_list=(MyAddressList)context.getBean("add_list");
	System.out.println("\nLIST IS:\n"+add_list);
	System.out.println("LIST SIZE:"+add_list.getAddresses().size());
	
	MyAddressSet add_set=(MyAddressSet)context.getBean("add_set");
	System.out.println("\nSET IS:\n"+add_set);
	System.out.println("SET SIZE:"+add_set.getAddresses().size());
	
	MyAddressMap add_map=(MyAddressMap)context.getBean("add_map");
	System.out.println("\nMAP IS:\n"+add_map);
	System.out.println("MAP SIZE:"+add_map.getAddresses().size());
	
	Country add_prop=(Country)context.getBean("add_property");
	System.out.println("\nPROPERTY IS:\n"+add_prop);
	System.out.println("PROPERTY STATE SIZE:"+add_prop.getState_capitals().size());
	
	MyAddressList add_list_util=(MyAddressList)context.getBean("add_list_util");
	System.out.println("\nUTIL LIST IS:\n"+add_list_util);
	System.out.println("UTIL LIST SIZE:"+add_list_util.getAddresses().size());
	
	MyAddressSet add_set_util=(MyAddressSet)context.getBean("add_set_util");
	System.out.println("\nUTIL SET IS:\n"+add_set_util);
	System.out.println("UTIL SET SIZE:"+add_set_util.getAddresses().size());
}
}
