
public class Employee 
{
	private int empid;
	private String name;
	public Employee() {
		// TODO Auto-generated constructor stub
		//System.out.println("EMP CONSTRUCTOR DEF");		
	}
	public Employee(int empid, String name) {
		//System.out.println("EMP CONSTRUCTOR PARAM");		
		this.empid = empid;
		this.name = name;
	}
	public int getEmpid() {
		return empid;
	}
	public void setEmpid(int empid) {
		this.empid = empid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	} 
	public void Display()
	{
		System.out.println("EmpId: "+empid+" Name: "+name);
	}
	@Override
	public String toString() {
		return "Employee [empid=" + empid + ", name=" + name + "]";
	}
	@Override
	public boolean equals(Object arg0) {
		// TODO Auto-generated method stub
		Employee e1=(Employee)arg0; //Downcasting
		return this.empid==e1.getEmpid() && this.name.equals(e1.getName());
		//return this.empid==e1.getEmpid() && this.name==e1.getName();
	}
	public static void MyData()
	{
		System.out.println("STATIC METHOD EMP");
	}
//	@Override
//	public String toString() {
//		// TODO Auto-generated method stub
//		String s="EmpId: "+empid+"\tName: "+name;
//		return s;
//		
//	}
}
