package com.mockito;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class TestMyDemo {

	MyDemo mydemo;
	@Before
	public void setUp() throws Exception {
		mydemo=mock(MyDemo.class);//DummyObject
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetData() {
		when(mydemo.getData(10)).thenReturn(200f);//10
		when(mydemo.getData(50)).thenCallRealMethod();//30
		assertEquals(30f,mydemo.getData(50),0);               //assertEquals(expected, actual, delta);
		assertEquals(200f,mydemo.getData(10),0);
	}

	@Test
	public void testFormMessage() {
		when(mydemo.formMessage("hello")).thenReturn("Hi");
		when(mydemo.formMessage("anuja")).thenCallRealMethod();
		assertEquals("hello anuja", mydemo.formMessage("anuja"));
		assertEquals("Hi", mydemo.formMessage("hello"));
	}

}
