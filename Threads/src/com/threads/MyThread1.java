package com.threads;

public class MyThread1 extends Thread {
	public static void main(String[] arg) {
	
		System.out.println("start");
		System.out.println("Thread : "+Thread.currentThread().getName());
		
		Thread t=new MyThread1();
		t.start();
		
		Thread t2=new MyThread1();
		t2.start();
		
		Thread t3=new MyThread1();
		t3.start();
		
		Thread t4=new MyThread1();
		t4.setName("Mythread_anu");
		t4.start();
	
		System.out.println("end");
		
	}
	@Override
	public void run()
	{
		System.out.println("Thread in RUN: "+Thread.currentThread().getName());
	}
}
