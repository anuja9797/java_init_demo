package com.wiring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.contain.Employee;

public class Client_Wiring {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ApplicationContext context=new ClassPathXmlApplicationContext("wiring.xml");//load factory and create object
		Employee emp=(Employee)context.getBean("empw");
		System.out.println(emp);
	}

}
