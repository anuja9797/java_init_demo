package com.myBeans;

import org.springframework.beans.factory.InitializingBean;

public class DemoInitializingBean implements InitializingBean{
private String message;
private String name;

public DemoInitializingBean() {
	// TODO Auto-generated constructor stub
	System.out.println("-----------------------------------");
	System.out.println("DEFAULT CONSTRUCTOR");
	message="Welcome!";
	name="CLSA";
			
}

@Override
public String toString() {
	return message+"\t"+name;
}

public String getMessage() {
	return message;
}

public void setMessage(String message) {
	this.message = message;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}
public void myInit()
{
	name=name.toUpperCase();
	message=message.toUpperCase();
	System.out.println("myInit() METHOD CALLED");
}

public void myDestroy()
{
	name=null;
	message=null;			
	System.out.println("myDestroy() METHOD CALLED");
}

@Override
public void afterPropertiesSet() throws Exception {
	// TODO Auto-generated method stub
	System.out.println("afterPropertiesSet() METHOD CALLED");
	message="welcomeee";
	name="clsa ltd";
}
}
