import java.util.Arrays;
import java.util.List;

import com.pojo.Student;

public class DemoParallelStreams {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Student> students=Arrays.asList(
				new Student(1,"anuja",80.45d),
				new Student(2,"nikita",81.44d),
				new Student(3,"juilee",82.43d),
				new Student(4,"apurva",83.42d),
				new Student(4,"apurva",83.42d),
				new Student(4,"apurva",83.42d)
				);
		long t1=System.currentTimeMillis();
		System.out.println("Sequential Stream cnt= "+
		students.stream().filter(e -> {
			System.out.println(Thread.currentThread().getName());
			return e.getPercentage()>=80;
			}).count());
		
		long t2=System.currentTimeMillis();
		System.out.println("Sequential Stream Time Taken= "+(t2-t1));
		
		t1=System.currentTimeMillis();
		System.out.println("Parallel Stream cnt= "+students.parallelStream().filter(e -> {
			System.out.println(Thread.currentThread().getName());
			return e.getPercentage()>=80;}).count());
		
		t2=System.currentTimeMillis();
		System.out.println("Parallel Stream Time Taken= "+(t2-t1));
	}

}
