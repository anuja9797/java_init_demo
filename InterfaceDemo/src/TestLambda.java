
public class TestLambda {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Mylamda1 lamda=()->{System.out.println("WELCOME TO LAMBDA");}; //sysout->DEFINITION
		lamda.display();
//		Mylamda1 lamda1=()->System.out.println("WELCOME TO LAMBDA"); //can eliminate {}
		
		
		
		MyLambda2 lamda2=(a,name)->{System.out.println("Value is "+a+" Name "+name);
									System.out.println("Len of string: "+name.length());};
		//MyLambda2 lamda2=(int a,String name)->{System.out.println("Value is "+a+" Name "+name);};  //int can be eliminated
		lamda2.showMessage(12,"anuja");
		
		
		PersonLambda per=(Person p)->{
									System.out.println(p);
		};
		Person p=new Person("Anuja",21,new Address("Nashik","422003"));
		per.data(p);
		
		
		EmployeeLamda emp=(int id,String name)->{
			return new Employee(id,name);
		};
		
		Employee e1=emp.createEMp(10,"Juilee");
		System.out.println("EMPID: "+e1.getEmpid());
		e1.Display();
		
		System.out.println(emp.createEMp(11,"Nikita"));
		
		System.out.println(emp.createEMp(10,"Anuja").getName());
		
		
	}

}
