package com.singleton;

public class Person_Lazy {

	private String name;
	private static  Person_Lazy lazy;
	
	private Person_Lazy()
	{
		name="lazy";
	}
	public static Person_Lazy getInstance()
	{
		if(lazy==null)
		{
			lazy=new Person_Lazy();
		}
		return lazy;
	}
	@Override
	public String toString() {
		return "Person_Lazy [name=" + name + "]";
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Person_Lazy p=Person_Lazy.getInstance();
		System.out.println(p);
	}

}
//class Out
//{
//	void Test()
//	{
//		Person_Lazy p=Person_Lazy.getInstance();
//		System.out.println(p);
//	}
//}