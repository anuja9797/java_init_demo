import java.io.FileReader;
import java.io.IOException;

public class DemoThrows {
	
		
	    	void readfile()throws IOException
	    	{
			FileReader file=new FileReader("abc.txt");
	    	}
}
//class can throw can throw unchecked exceptions
class child extends DemoThrows
{
	//either surround with try catch
	@Override
	void readfile() 
	{
		try {
			Class.forName("myclass");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	//or throw
	void readfile(String msg) throws ClassNotFoundException 
	{
		Class.forName("myclass");
	}
}