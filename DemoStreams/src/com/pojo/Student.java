package com.pojo;

import java.util.Arrays;

public class Student {
private int rollNo;
private String name;
private double percentage;
private int total;
private int[] marks;

@Override
public String toString() {
	return "Student [rollNo=" + rollNo + ", name=" + name + ", percentage=" + percentage + ", total=" + total
			+ ", marks=" + Arrays.toString(marks) + "]";
}

public int getRollNo() {
	return rollNo;
}

public void setRollNo(int rollNo) {
	this.rollNo = rollNo;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public double getPercentage() {
	return percentage;
}

public void setPercentage(double percentage) {
	this.percentage = percentage;
}

public int getTotal() {
	return total;
}

public void setTotal(int total) {
	this.total = total;
}

public int[] getMarks() {
	return marks;
}

public void setMarks(int[] marks) {
	this.marks = marks;
}

public Student() {
	// TODO Auto-generated constructor stub
}

public Student(int rollNo, String name, double percentage) {
	super();
	this.rollNo = rollNo;
	this.name = name;
	this.percentage = percentage;
}

public Student(int rollNo, String name, int[] marks) {
	super();
	this.rollNo = rollNo;
	this.name = name;
	this.marks = marks;
}


}
