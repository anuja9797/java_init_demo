import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.pojo.Employee;

public class TestDemoEmployee {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}


//	@Test()
//	public void testAddEmployee_POSITIVE() {
//		try {
//			DemoEmployee obj=new DemoEmployee();
//			Employee e=new Employee(980,"abcd","pune",30,30,"dev");
//			int a=obj.addEmployee(e);
//			System.out.println(a);
//			assertEquals(1,a);
//		} catch (ClassNotFoundException | SQLException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//		
//	}

//	@Test(expected=java.sql.SQLIntegrityConstraintViolationException.class)
//	public void testAddEmployee_POSITIVE() throws SQLException{
//		try {
//			DemoEmployee obj=new DemoEmployee();
//			Employee e=new Employee(945,"abcd","pune",30,30,"dev");
//			int a=obj.addEmployee(e);
//			System.out.println(a);
//			assertEquals(1,a);
//		} catch (ClassNotFoundException | SQLException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//		
//	}        //for negative test cases
	
//	@Test
//	public void testAddEmployee_NEGATIVE() {
//		try {
//			DemoEmployee obj=new DemoEmployee();
//			Employee e=new Employee(997,"newname","pune",30,30,"dev");
//			int a=obj.addEmployee(e);
//			assertEquals(0,a);
//		} catch (ClassNotFoundException | SQLException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//		
//	}

//	@Test
//	public void testUpdateEmployee_POSITIVE() {
//		
//		try {
//			DemoEmployee obj=new DemoEmployee();
//			Employee emp=new Employee(997,"newname","pune",30,30,"dev");
//			boolean a=obj.updateEmployee(997, emp);
//			assertEquals(true,a);
//		} catch (ClassNotFoundException | SQLException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//
//	}
	@Test
	public void testUpdateEmployee_POSITIVE() {    //if no return value of method
		
		try {
			DemoEmployee obj=new DemoEmployee();
			Employee emp=new Employee(997,"newname","newpune",30,30,"dev");
			obj.updateEmployee(997, emp);
			
			assertEquals(emp.getEmpId(),997);
			assertTrue(emp.getEmpName().equals("newname"));
			assertEquals(emp.getEmpAddress(),"newpune");
			assertEquals(emp.getEmpAge(),30);
			assertEquals(emp.getEmpExperience(),30);
			assertEquals(emp.getDept(),"dev");
			
		
			
			
		} catch (ClassNotFoundException | SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}
//	@Test
//	public void testUpdateEmployee_NEGATIVE() {
//		
//		try {
//			DemoEmployee obj=new DemoEmployee();
//			Employee emp=new Employee(77,"newname","pune",30,30,"dev");
//			boolean a=obj.updateEmployee(997, emp);
//			assertEquals(false,a);
//		} catch (ClassNotFoundException | SQLException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//
//	}
	
	@Test
	public void testUpdateEmployee_NEGATIVE() {    //if no return value of method
		
		try {
			DemoEmployee obj=new DemoEmployee();

			Employee emp1=new Employee(77,"newname","pune",30,30,"dev");

			obj.updateEmployee(77, emp1);



			Employee emp=obj.getEmployeeById(77);

			assertNotEquals(emp.getEmpId(),77);
		
			
			
		} catch (ClassNotFoundException | SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}
	@Test
	public void testGetEmployeeById_POSITIVE() {
		try {
			DemoEmployee obj=new DemoEmployee();
			Employee emp=obj.getEmployeeById(997);
//			Employee emp1=new Employee(997,"newname","pune",30,30,"dev");
//			assertEquals(emp,emp1);
			assertEquals(emp.getEmpId(),997);
			assertTrue(emp.getEmpName().equals("newname"));
			assertEquals(emp.getEmpAddress(),"newpune");
			assertEquals(emp.getEmpAge(),30);
			assertEquals(emp.getEmpExperience(),30);
			assertEquals(emp.getDept(),"dev");
			
		} catch (ClassNotFoundException | SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}
	@Test
	public void testGetEmployeeById_NEGATIVE() {
		try {
			DemoEmployee obj=new DemoEmployee();
			Employee emp=obj.getEmployeeById(669);
//			Employee emp1=new Employee(997,"newname","pune",30,30,"dev");
//			assertEquals(emp,emp1);
			System.out.println(emp);
			assertEquals(emp.getEmpId(),0);
			
			
		} catch (ClassNotFoundException | SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	@Test
	public void testGetAllEmployee() throws ClassNotFoundException, SQLException {
		DemoEmployee obj=new DemoEmployee();
		
		List<Employee> list=obj.getAllEmployee();
		
		assertEquals(7, list.size());
		int flag=0;
		Iterator<Employee> itr=list.iterator();
		while(itr.hasNext())
		{
			Employee emp=itr.next();
			if(emp.getEmpId()==980) {
			assertEquals(emp.getEmpId(),980);
			assertTrue(emp.getEmpName().equals("abcd"));
			assertEquals(emp.getEmpAddress(),"pune");
			assertEquals(emp.getEmpAge(),30);
			assertEquals(emp.getEmpExperience(),30);
			assertEquals(emp.getDept(),"dev");
			flag=1;
			}
		}
		
		assertEquals(1,flag);
	}

	@Test
	public void testGetAllEmployeeByExp() {
		
	}

	@Test
	public void testGetAllEmployeeByDept() {
		
	}

}
