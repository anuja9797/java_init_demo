import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DemoList1 {

	@SuppressWarnings({ "unchecked", "unused", "rawtypes" })
	public static void main(String[] args) {
		// TODO Auto-generated method stub

			List<Integer> intlist=new ArrayList();
			intlist.add(100);intlist.add(200);intlist.add(300);intlist.add(400);intlist.add(100);
			System.out.println(intlist.size());
			System.out.println(intlist);
			
			Integer obj=new Integer(99);//BOXING-UPCASTING
			intlist.add(5,obj);			 
//			intlist.add(5,"new");
//			intlist.add(5,new Thread());
//			intlist.add(5,1.02f);
			
			Iterator<Integer> itr=intlist.iterator();
		
//			while(itr.hasNext())
//			{	Integer i=itr.next();
//				//Integer i=itr.next();//Downcasting not needed
//				//System.out.println(i);
//				if(i>100) 
//				{
//					System.out.println(i);
//				}
//			}
			for(int value:intlist)
			{
				System.out.println(value);
			}
			System.out.println("1st element: "+intlist.get(0));
			intlist.remove(0);//using index
			intlist.remove(new Integer("200")); //using value=>Cast it into Object
			System.out.println(intlist);
	}

}
