<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<jstl:if test="${not empty book_list }">
		<h1 align="center">
			<font color="green">BOOK LIST OF ${auth_name }</font>
		</h1>
		<table align="center" border="1">
			<tr>
				<th>NAME</th>
				<th>ISBN</th>
				<th>AUTHOR</th>
				<th>PUBLICATION</th>
				<th>PRICE</th>
				<th>DESCRIPTION</th>
			</tr>
			<jstl:forEach var="book_data" items="${book_list }" varStatus="st">
				<tr>
					<td><jstl:out value="${ book_data.bookName }"></jstl:out></td>
					<td><jstl:out value="${ book_data.ISBN }"></jstl:out></td>
					<td><jstl:out value="${ book_data.author }"></jstl:out></td>
					<td><jstl:out value="${ book_data.publication }"></jstl:out></td>
					<td><jstl:out value="${ book_data.price }"></jstl:out></td>
					<td><jstl:out value="${ book_data.description }"></jstl:out></td>
				</tr>
			</jstl:forEach>
		</table>
	</jstl:if>
	<jstl:if test="${empty book_list }">
		<jstl:out value="NO BOOK FOUND"></jstl:out>
	</jstl:if>
</body>
</html>