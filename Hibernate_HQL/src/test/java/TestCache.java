import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import com.ehcache.Address;



public class TestCache {

	public static void main(String[] args) {		
	Configuration configuration = new Configuration().configure();
	StandardServiceRegistry serviceRegistry=new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
	Metadata meta = new MetadataSources(serviceRegistry).getMetadataBuilder().build();
	SessionFactory sessionFactory = meta.getSessionFactoryBuilder().build();
		

		
/*		SessionFactory sessionFactory = configuration.buildSessionFactory()
 		Session session = sessionFactory.openSession();
 		session.beginTransaction();
		Address add=new Address();			add.setCity("Nashik");			add.setPincode(123456l);
		session.save(add);
		Address add1=new Address();			add1.setCity("Pune");			add1.setPincode(200056l);
		session.save(add1);
		Address add2=new Address();			add2.setCity("Mumbai");			add2.setPincode(9853241l);
		session.save(add2);
		session.getTransaction().commit();
		
		*/
		Session session = sessionFactory.openSession();
		Address addr=session.get(Address.class, 1);
		System.out.println(addr);
		session.close();
		
		Session session2 = sessionFactory.openSession();
		Address addr2=session2.get(Address.class, 1);
		System.out.println(addr2);
		session2.close();
		System.out.println(System.getProperty("java.io.tmpdir"));

		
		
		
		System.out.println("SAVED");}
	
}
