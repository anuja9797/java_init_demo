package com.beans;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.pojo.Student;

public class TestStudentStreams {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Student> students1=Arrays.asList(
				new Student(19,"anuja",new int[] {80,90,85}),
				new Student(18,"anuja",new int[] {80,90,85}),
				new Student(17,"anuja",new int[] {80,90,85}),
				new Student(12,"nikita",new int[] {80,95,91}),
				new Student(33,"juilee",new int[] {82,92,85}),
				new Student(85,"juilee",new int[] {82,92,85}),
				new Student(14,"apurva",new int[] {90,90,95})
				);
		
		List<Integer> total=calculateTotal(students1);
		System.out.println(total);
		
		List<Student> newstud=calculateAvg(students1);
		newstud.forEach((s)->{System.out.println(s);});
		
		System.out.println();
		List<Student> sortstud=sortRollNo(students1);
		sortstud.forEach((s)->{System.out.println(s);});
		
		System.out.println();
		List<Student> sortstud_name=sortName(students1);
		sortstud_name.forEach((s)->{System.out.println(s);});
		
		//Grouping
		Map<String,List<Student>> map=new HashMap<>();
		map=groupByName(students1);
		map.forEach((key,value)->
		{
			System.out.println("------------------------\nKEY: "+key);
			value.forEach((s)->{System.out.println(s.getName()+" "+s.getRollNo());});
		});
		
		
		//Partitioning
		
		Predicate<Student> pred = s -> s.getPercentage() >= 88;
		Map<Boolean,List<Student>> partition_per = students1.stream().collect(Collectors.partitioningBy(pred));
		
		
		//TO DISPLAY
		partition_per.forEach((key,value)->
		{
			System.out.println("------------------------\nKEY: "+key);
			value.forEach((s)->{System.out.println(s);});
		});
		//OR
//		Set<Entry<Boolean,List<Student>>> setQ =partition_per.entrySet();
//		setQ.stream().forEach((entry) -> {
//			System.out.println(entry.getKey()+"\t"+entry.getValue());
//		});
		
	}
    public static List<Integer> calculateTotal(List<Student> students)
    {
    	return students.stream().map((s) -> {
    		int total=0;
    		for(int mark:s.getMarks())
    		{
    			total+=mark;
    		}
    		s.setTotal(total);
    		return total;
    	}).collect(Collectors.toList());
    }
    public static List<Student> calculateAvg(List<Student> students)
    {
    	return students.stream().map((s) -> {
    		double len=s.getMarks().length;
    		int total=0;
    		for(int mark:s.getMarks())
    		{
    			total+=mark;
    		}
    		s.setTotal(total);
    		s.setPercentage(total/len);
    		return s;
    	}).collect(Collectors.toList());
    }
    
    public static List<Student> sortRollNo(List<Student> students)
    {
    	//also using a comparator
    	return students.stream().sorted((stu1,stu2)->{
    		return stu1.getRollNo()-stu2.getRollNo();
    		//returns a stream consisting of the elements of this stream in sorted order
    	}).collect(Collectors.toList());
    }
    public static List<Student> sortName(List<Student> students)
    {
    	return students.stream().sorted((stu1,stu2)->{
    		return stu1.getName().compareTo(stu2.getName());
    		//returns a stream consisting of the elements of this stream in sorted order
    	}).collect(Collectors.toList());
    }
    
    public static Map<String,List<Student>> groupByName(List<Student> students)
    {
    	return students.stream().collect(Collectors.groupingBy(Student::getName));
    	/*The classification function passed to groupingBy() method is the method reference 
    	  to Student.getName() by Student::getName */
    	
    }
}
