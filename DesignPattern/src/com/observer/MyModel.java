package com.observer;


import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

public class MyModel {

	public static final String NAME="lastname";
	private List<Data> data;
	
	private List<PropertyChangeListener> listener =new ArrayList<PropertyChangeListener> ();
	
	public MyModel()
	{
		data=new ArrayList<Data>();
		data.add(new Data(10,"name1"));
		data.add(new Data(20,"name2"));
		data.add(new Data(20,"name3"));
		data.add(new Data(20,"name4"));
		data.add(new Data(20,"nam5"));
		System.out.println("Data added");
	}
	class Data
	{
		int value;
		String name;
		public Data() {
			// TODO Auto-generated constructor stub
		}
		@Override
		public String toString() {
			return "Data [value=" + value + ", name=" + name + "]";
		}
		public int getValue() {
			return value;
		}
		public void setValue(int value) {
			this.value = value;
		}
		public String getName() {
			return name;
		}
		public Data(int value, String name) {
			super();
			this.value = value;
			this.name = name;
		}
		public void setName(String name) {
			
			notifyListeners(this, name, this.name,name);
			this.name = name;
			//System.out.println("Set");
		}
		
	}
	
	public List<Data> getData()
	{
		return data;
	}
	public void setData(List<Data> data)
	{
		this.data=data;
	
	}
	
	private void notifyListeners(Object obj,String property,String oldval,String newval)
	{
		for(PropertyChangeListener name:listener)
		{
			name.propertyChange(new PropertyChangeEvent(this,property,oldval,newval));
		}
	}
	
	
	
	public void addChangeListener(PropertyChangeListener newlistener) {
		listener.add(newlistener);
	}

}

/*
1.MyModel obj created
2.Data is added in list<Data> data in MyModel constructor
3.MyObserver object is created and in constructor mymodel obj is attached to it
4. data.setname() is called for every element in list in main()
5.setname calls notifyListeners(obj,property,old,new)
6.In notify listener, for every listener(here,only MyObserver), property change method is called
7.In MyObserver PropertyChange method, msg is displayed
 **/
