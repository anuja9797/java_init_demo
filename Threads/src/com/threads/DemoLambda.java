package com.threads;

import java.util.concurrent.Callable;

public class DemoLambda {

	public static void main(String[] args) throws Exception{
		// TODO Auto-generated method stub
		Runnable r=()->{
			System.out.println(Thread.currentThread().getName());
			//return 0; cannot return any value
			};
		Thread thread=new Thread(r);
		thread.start();
		
		Callable<String> callable=()->{return "hello";};//call method def=>also can throw exception
		System.out.println("value "+callable.call());
	}

}
