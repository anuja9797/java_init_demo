import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class DemoCreateTable {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Step1: Load and Register the driver 
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			System.out.println("DRIVER LOADED");	
			Connection conn=DriverManager.getConnection("jdbc:oracle:thin:@vhkdld387:1551:staffd","hr","Eagle#2019"); //url, username, pwd
			System.out.println("Connected");
			
			String CREATE_TABLE="CREATE TABLE ANU_MYDATA(ID INTEGER,NAME VARCHAR(20))";
			Statement st=conn.createStatement();
			int ret=st.executeUpdate(CREATE_TABLE);
			System.out.println("TABLE CREATED");
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
