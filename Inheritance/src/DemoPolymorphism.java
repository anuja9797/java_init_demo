import java.lang.reflect.InvocationTargetException;

public class DemoPolymorphism {

	public static void main(String[] args) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, InstantiationException {
		//ref to child=Object of child
		Developer d=new Developer();
		System.out.println(d);//calls dev toString
		//ref to parent=Object of Child/Parent
		Employee e=new Employee();
		System.out.println(e);//calls emp toString
		e=new Developer();
		System.out.println(e);//calls dev toString
		//calls emp toString 
		System.out.println(d.getClass().getSuperclass().getMethod("toString", new Class[]{}).invoke(d.getClass().getSuperclass().newInstance() ,new Object[]{})) ;
//		e.Display();
//		((Developer)e).Display("name");
//		d.Display();
//		d.Display("name");
		
		
//		d.MyData();//calls Dev method as ref=dev
//		e.MyData();//calls Emp method as ref=emp
		//Static method depends on Reference
	}
}
