package com.beans;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
//Table(name="AnuMyClass")
public class AnuMyClass {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)//if strategy (AUTO,SQE,etc) is selected then param const is not usedo
	private int classId;
	private String className;
	private double value;
	
	public AnuMyClass() {
		// TODO Auto-generated constructor stub
	}

//	public AnuMyClass(int classId, String className, double value) {
//		super();
//		this.classId = classId;
//		this.className = className;
//		this.value = value;
//	}                      //SHOULD NOT BE USED 

	public int getClassId() {
		return classId;
	}

	public void setClassId(int classId) {
		this.classId = classId;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "MyClass [classId=" + classId + ", className=" + className + ", value=" + value + "]";
	}
	
}
