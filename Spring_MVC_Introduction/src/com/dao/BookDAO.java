package com.dao;

import java.util.List;

import com.pojo.Book;

public interface BookDAO {
	
	public int addBook(Book book);
	public int updateBook(long ISBN,int price);
	public boolean deleteBook(long ISBN);
	public List<Book> findAllBooks();
}
