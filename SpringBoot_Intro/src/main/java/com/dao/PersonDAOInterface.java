package com.dao;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.beans.Person;
public interface PersonDAOInterface {
	public int addPerson(Person Person);
	public Person getPerson(int id);
	public Person deletePerson(int id);
	public List<Person> getAllPersons_1();
}
//@Repository
//public interface PersonDAOInterface extends JpaRepository<Person, Integer>
//{
//	public List<Person> findByName(String name);
////	public List<Person> findByFirstName(String firstName);
//}