
public class DemoOverloading {
	 void display()
	{
		System.out.println("Display0");
	}
	 public void display(int a)
	{
		System.out.println("Display "+a);
	}
//	 public void display(int a,int b)
//	{
//		System.out.println("Display "+a+" "+b);
//	}
	 public void display(float a,int b)
	{
		System.out.println("Display "+a+" "+b);
	}
	 public void display(int a,float b)
	{
		System.out.println("Display "+a+" "+b);
	}
	 public void display(int...b) //one or more
	{	System.out.println("Display one or more ");
		for(int v :b)
			System.out.println(v);
	}
	public void display(String s,int...b) //one or more
	{
		System.out.println("Display one or more "+s);
		for(int v :b)
			System.out.println(v);
	}
	 public static void main(String[] args) {
		// TODO Auto-generated method stub
		DemoOverloading d=new DemoOverloading();
		d.display();
		d.display(1);
		d.display(1,2.0f);
		d.display(2.0f,1);
		//display(2,3) clashes with d(float,int) and d(int...x)
		d.display(1,2,0);
		d.display("hello",2,1);
		d.display("hi",2,3,1);
	}

}
