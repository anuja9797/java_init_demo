package com.inheritance;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.pojo.Person;

public class Client {

	public static void main(String[] args) {
		ApplicationContext context=new ClassPathXmlApplicationContext("beans.xml");//load factory and create object
//		Parent p=(Parent)context.getBean("parent");
//		System.out.println(p);  
		//Parent is used for internal purpose only if it is abstract
		Child c=(Child)context.getBean("child");
		System.out.println(c);
		Child c1=(Child)context.getBean("child1");
		System.out.println(c1);
		
		
		MyClass m=(MyClass)context.getBean("myclass");
		System.out.println(m);
	}
}
