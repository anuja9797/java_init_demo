package com.executorservice;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class DemoCallable1 {
public static void main(String[] args) throws InterruptedException, ExecutionException{
	// TODO Auto-generated method stub

	//---------------------------1st approach
	Callable<Integer> task=()->{
		try {
			System.out.println("task submitting  ");
			
			TimeUnit.SECONDS.sleep(3);
			return 100;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	};
	ExecutorService service=Executors.newFixedThreadPool(2);
	Future<Integer> future=service.submit(task);
	System.out.println("Is Future Done "+future.isDone());
	System.out.println("Is Future Done "+future.isDone());

	Integer result=future.get();//waits till call finishes exceuted
	
	System.out.println("Is Future Done "+future.isDone());
	System.out.println("Is Future Done "+future.isDone());
	System.out.println("Is Future Done "+future.isDone());
	System.out.println(result);
	
	service.shutdown();
	
	
	//---------------------------2nd approach
	
	service=Executors.newWorkStealingPool();
	List<Callable<String>> clist=Arrays.asList(
			()->"task1",
			()->"task2",
			()->"task3"
			);
	
	service.invokeAll(clist).stream().map(
			future1 -> {try {
				return future1.get();
			}catch(Exception c)
			{
				throw new IllegalStateException(c);
			}
			}).forEach(System.out::println);
	//SERVICE.INVOKE=>callable list->STREAM->callable->MAP->{future,str(future.get())->FOREACH->str}
	
	}
	
}

