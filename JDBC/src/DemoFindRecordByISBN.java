import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class DemoFindRecordByISBN {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Step1: Load and Register the driver 
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			System.out.println("DRIVER LOADED");	
			Connection conn=DriverManager.getConnection("jdbc:oracle:thin:@vhkdld387:1551:staffd","hr","Eagle#2019"); //url, username, pwd
			System.out.println("Connected");
			
			Scanner inp=new Scanner(System.in);
		
			System.out.println("Enter ISBN:");
			int isbn1=inp.nextInt();
//			String FIND_ALL_BOOKS=" SELECT * FROM ANU_BOOK  WHERE ISBN="+isbn1;
//			Statement ps=conn.createStatement();
			String FIND_ALL_BOOKS=" SELECT * FROM ANU_BOOK  WHERE ISBN=?";
			PreparedStatement ps=conn.prepareStatement(FIND_ALL_BOOKS);
			ps.setInt(1,isbn1);
			ResultSet rs=ps.executeQuery();
			while(rs.next())
			{
				int isbn=rs.getInt(1);
				String name=rs.getString("name");
				String author=rs.getString(3);
				int price=rs.getInt(4);
				System.out.println(isbn+"\t\t"+name+"\t\t"+author+"\t\t"+price);
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			
			if(e.getErrorCode()==00001)
				System.out.println("Primary Key");
			e.printStackTrace();
		}
	}
}
