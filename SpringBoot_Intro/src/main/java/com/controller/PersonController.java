package com.controller;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.beans.Person;
import com.service.PersonService;

@RestController//just like controller layer inSpringMVC
public class PersonController {

@Autowired
PersonService personService;//bean for this class initialised


		@GetMapping("/persons-data")
		public List<Person> getPersons_data()
		{
				System.out.println("found");
				List<Person> persons=personService.getAllPersons();
				return persons;
				//Run as Java application SpringBootIntroductionApplication.java and the on browser type http://localhost:8080/persons-data
				//All person data returned on browser therefore always return
		}
	
	
	
//		@GetMapping("/persons-data/{id}")//When declaring uri then use{} ,in browser no {}
//		public Person getPersons_data(@PathVariable int id)//Pathvaraible
//		{
//			
//			System.out.println("found");
//			Person p=personService.getPerson(id);
//			return p;
//			//data for id=1 returned in json/xml format on bowser
//			//Run http://localhost:8080/persons-data/1
//		}
		
		
		@PostMapping("/persons-data")//to add data write PostMapping
		public Person addPersonData(@RequestBody Person p)
		{
				System.out.println("found");
				Person p1=personService.addPerson(p);
				return p1;
				//Run POST http://localhost:8080/persons-data/
		}
		
		
		
		
//		@GetMapping("/persons-data-entity/{id}")
//		public ResponseEntity<Person> findById(@PathVariable int id)//Pathvaraible
//		{
//		
//				System.out.println("found");
//				Person p=personService.getPerson(id);
//				Optional<Person> optional=Optional.of(p);
//				if(optional.get().getPersonId()!=0)
//					return new ResponseEntity<Person>(optional.get(),HttpStatus.OK);//On POSTMAN status column will have 200OK
//				return new ResponseEntity(HttpStatus.NO_CONTENT);//On POSTMAN status column will have 204 NO CONTENT
//				//data for id=1 returned in json/xml format on bowser or if some other id passed it returns NO CONTENT
//				//Run http://localhost:8080/persons-data-entity/1
//		}
//		@GetMapping("/persons-data-name/{name}")
//		public List<Person> findByName(@PathVariable String name)//Pathvaraible
//		{
//		
//				System.out.println("found");
//				List<Person> p=personService.getPerson_name(name);
//				return p;
//		}
		
		//Validations
		@PostMapping("/persons-data1")
		public Person addPersonData1(@Valid@RequestBody Person p)
		{
				System.out.println("found");
				Person p1=personService.addPerson(p);//personService bean
				return p1;
		}


}