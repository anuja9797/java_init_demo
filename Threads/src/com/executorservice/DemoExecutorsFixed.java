package com.executorservice;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class DemoExecutorsFixed {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ExecutorService service=Executors.newFixedThreadPool(4);
		Runnable r=()->{
			System.out.println("I'm Runnable  "+Thread.currentThread().getName());
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		};
		for(int i=0;i<10;i++) {
		Future future=service.submit(r);//submit when RUNNABLE OR CALLABLE IS KNOWN
		//service.execute(r);//else
		System.out.println("future:  "+future);
		try {
			System.out.println(future.get()); //returns null=>successful completion
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
		service.shutdown();
	}

}
