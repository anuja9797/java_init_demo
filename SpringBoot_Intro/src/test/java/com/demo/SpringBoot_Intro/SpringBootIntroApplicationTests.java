package com.demo.SpringBoot_Intro;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import com.beans.Person;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBootIntroApplicationTests {

	@Test
	public void contextLoads() {
		RestTemplate template=new RestTemplate();
		SimpleDateFormat dateformat =new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
		Date newdate=null;
		String strdate="02-04-2018 11:35:42";
		try {
			newdate = dateformat.parse(strdate);
			System.out.println(newdate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//		Person person=new Person();
//		person.setName("person_test2");
//		person.setBirthDate(newdate);
//		Person person_inserted = template.postForObject("http://localhost:8083/persons-data", person, Person.class);
//		System.out.println(person_inserted+" ADDED");
		Person person=new Person("person_test2",123456,newdate);
		Person person_inserted = template.postForObject("http://localhost:8083/persons-data", person, Person.class);
		System.out.println(person_inserted+" ADDED");
	    Assert.assertEquals("person_test2", person.getName());
	}

}
