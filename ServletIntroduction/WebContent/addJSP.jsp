<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<jsp:useBean id="emp" class="com.pojo.Employee"  scope="request"></jsp:useBean>
	<jsp:setProperty property="*" name="emp"/>
	
	<br>
	Name:<jsp:getProperty property="empName" name="emp"/><br>
	Id:<jsp:getProperty property="empId" name="emp"/><br>
	Age:<jsp:getProperty property="empAge" name="emp"/><br>
	Exp:<jsp:getProperty property="empExperience" name="emp" /><br>
	Address:<jsp:getProperty property="empAddress" name="emp"/><br>
	Department:<jsp:getProperty property="dept" name="emp"/><br>
	
	<jsp:forward page="showJSP.jsp"></jsp:forward>
</body>
</html>
<!-- 
	<jsp:useBean id="emp1" class="com.pojo.Employee"></jsp:useBean>
	<jsp:setProperty property="empName" name="emp" param="empName"/>
	<jsp:setProperty property="empId" name="emp" param="empId"/>
	<jsp:setProperty property="empAge" name="emp" param="empAge"/>
	<jsp:setProperty property="empExperience" name="emp" param="empExperience"/>
	<jsp:setProperty property="empAddress" name="emp" param="empAddress"/>
	<jsp:setProperty property="dept" name="emp" param="empDepartment"/>
	<br>
	Name:<jsp:getProperty property="empName" name="emp"/><br>
	Id:<jsp:getProperty property="empId" name="emp"/><br>
	Age:<jsp:getProperty property="empAge" name="emp"/><br>
	Exp:<jsp:getProperty property="empExperience" name="emp" /><br>
	Address:<jsp:getProperty property="empAddress" name="emp"/><br>
	Department:<jsp:getProperty property="dept" name="emp"/><br> -->