<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
EMPLOYEE DETAILS:<br>
	<jsp:useBean id="emp" class="com.pojo.Employee"  scope="request"></jsp:useBean>
	Name:<jsp:getProperty property="empName" name="emp"/><br>
	Id:<jsp:getProperty property="empId" name="emp"/><br>
	Age:<jsp:getProperty property="empAge" name="emp"/><br>
	Exp:<jsp:getProperty property="empExperience" name="emp" /><br>
	Address:<jsp:getProperty property="empAddress" name="emp"/><br>
	Department:<jsp:getProperty property="dept" name="emp"/><br>
</body>
</html>