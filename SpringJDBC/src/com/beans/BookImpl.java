package com.beans;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
@Component
public class BookImpl implements BookDAO {

//	ApplicationContext context;
	@Autowired
	DataSource dataSource;
	
	public static void main(String[] args) {
		ApplicationContext context=new ClassPathXmlApplicationContext("wiring.xml");
		Book b=new Book("a",1123123,"a","d","er",123);
		BookDAO obj=(BookDAO)context.getBean("bookImpl");
//		int r=obj.addBook(b);
//		if(r==1)
//			System.out.println("ADDED");
//		else
//			System.out.println("OOPS");
		
		int r=obj.updateBook(123456,500);
		if(r==1)
			System.out.println("UPDATED");
		else
			System.out.println("OOPS");
	}
	public BookImpl() {
		// TODO Auto-generated constructor stub
		//context=new ClassPathXmlApplicationContext("wiring.xml");
		//datasource=(DataSource)context.getBean("dataSource");
		//datasource.getConnection()
	}
	
	@Override
	public int addBook(Book book) {
		// TODO Auto-generated method stub
		String SQL_INSERT ="insert into anu_books values(?,?,?,?,?,?)";
		int row=0;
		try {
			Connection conn=dataSource.getConnection();
			PreparedStatement ps=conn.prepareStatement(SQL_INSERT);
			ps.setString(1,book.getBookName());
			ps.setLong(2, book.getISBN());
			ps.setString(3,book.getPublication());
			ps.setString(4,book.getDescription());
			ps.setString(5,book.getAuthor());
			ps.setInt(6, book.getPrice());
			row=ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return row;
	}

	@Override
	public int updateBook(long ISBN, int price) {
		// TODO Auto-generated method stub
		String SQL_UPDATE ="update anu_books set price=? where ISBN=?";
				
		int row=0;
		try {
			Connection conn=dataSource.getConnection();
			PreparedStatement ps=conn.prepareStatement(SQL_UPDATE);
			ps.setInt(1,price);
			ps.setLong(2,ISBN);
			System.out.println(SQL_UPDATE);
			row=ps.executeUpdate();
			System.out.println(row);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return row;
	}

	@Override
	public boolean deleteBook(long ISBN) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Book> findAllBooks() {
		// TODO Auto-generated method stub
		return null;
	}

	
}
