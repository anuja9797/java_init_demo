package com.beans;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ClientBookJDBCTemplate {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ApplicationContext context=new ClassPathXmlApplicationContext("beans.xml");
//		ApplicationContext context=new ClassPathXmlApplicationContext("wiring.xml");
		Book b=new Book("abc",979797,"ass","good","erj",180);
		BookDAO obj=(BookDAO)context.getBean("BookDAO_JdbcTemplate");
//		int r=obj.addBook(b);
//		if(r==1)
//			System.out.println("ADDED");
//		else
//			System.out.println("OOPS");
		
//		int r=obj.updateBook(b.getISBN(),500);
//		if(r==1)
//			System.out.println("UPDATED");
//		else
//			System.out.println("OOPS");
		
//		obj.deleteBook(1123123);
		
		List<Book> list=obj.findAllBooks();
		for(Book book:list)
		{
			System.out.println(book);
		}
	}

}
