package com.demo.SpringBoot_Intro;

import static org.junit.Assert.*;


import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.beans.Person;
import com.dao.PersonDaoImpl;
import com.service.PersonService;


@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class Spring_Mockito {

	@Mock
	private PersonDaoImpl impl;
	
	@InjectMocks
	private PersonService personService =new PersonService();

	@Autowired
	private MockMvc mockMvc;
	
	@Before

	public void setUp() throws Exception {

	}

	@After

	public void tearDown() throws Exception {

	}
	@Test
	public void test() {
		
		List<Person> list=Arrays.asList(new Person(),new Person());
		Mockito.when(impl.getAllPersons_1()).thenReturn(list);//dummies imp method
		assertEquals(2,personService.getAllPersons().size());//calls service method and checks desired output of service method with mock impl method
		
	}
	
	@Test
	public void test_addPerson() {
		Person person=new Person();
		person.setName("testname");
		person.setBirthDate(new Date());
		person.setPersonId(100);
//		Mockito.when(impl.addPerson(person)).thenReturn(1);
		Mockito.when(personService.addPerson(person)).thenReturn(new Person());
		
		String ex="Person [name=" + "testname" + ", personId=" + 100 + ", birthDate=" + new Date() + "]";
		RequestBuilder requestBuilder =MockMvcRequestBuilders.post("http://localhost:8083/persons-data").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON);
		
		MvcResult result;
		
		try {
			result=mockMvc.perform(requestBuilder).andReturn();
			MockHttpServletResponse response=result.getResponse();
			assertEquals(HttpStatus.CREATED.value(),response.getStatus());
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
				
		
		
	}

}
