import javax.persistence.CascadeType;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.beans.Demo;
import com.beans.association.Address_One_to_One;
import com.beans.association.Person_One_to_One;

public class TestOneToOne {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Configuration configuration = new Configuration().configure();
	    Address_One_to_One address=new Address_One_to_One();
		Person_One_to_One p=new Person_One_to_One();
		Person_One_to_One p2 = null;
		
		
		try (SessionFactory sessionFactory = configuration.buildSessionFactory();
			Session session = sessionFactory.openSession();) {
			
			     session.beginTransaction();
			     
			    
			     address.setCity("NSK");
			     address.setPincode(123456l);
			     p.setName("NIKITA");
			     p.setAddress(address);
//			     session.save(address);  //NOT REQ if cascading option is selected (cascade=CascadeType.ALL)
//			     session.save(p);       //INSERT INTO TABLE
			     
			    p2=(Person_One_to_One)session.get(Person_One_to_One.class,4);
			     System.out.println("NAME IS "+p2.getName());
			   //System.out.println("CITY IS "+p2.getAddress().getCity());  //address invoked
			     session.getTransaction().commit();

			session.beginTransaction();
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		 System.out.println("CITY IS "+p2.getAddress().getCity());
		//IF LAZY, ADDRESS CANNOT BE FETCHED UNLESS INVOKED IN SESSION
	}

}

/*---------------------- LAZY

WHEN BOTH PERSON AND ADDRESS IS INVOKED IN SYSOUT
 			System.out.println("NAME IS "+p2.getName());
			System.out.println("CITY IS "+p2.getAddress().getCity());
			     
Hibernate: select person_one0_.personId as personId1_1_0_, person_one0_.address_id as address_id3_1_0_, person_one0_.name as name2_1_0_ from Anu_PersonOnetoOne person_one0_ where person_one0_.personId=?
Hibernate: select address_on0_.id as id1_0_0_, address_on0_.city as city2_0_0_, address_on0_.pincode as pincode3_0_0_ from Anu_AddressOnetoOne address_on0_ where address_on0_.id=?
Person is : 
Person_One_to_One [personId=4, name=NIKITA, address=Address_One_to_One [id=5, pincode=123456, city=NSK]]
NAME IS NIKITA
CITY IS NSK

WHEN ONLY PERSON IS INVOKED IN SYSOUT
 			System.out.println("NAME IS "+p2.getName());
 			
Hibernate: select person_one0_.personId as personId1_1_0_, person_one0_.address_id as address_id3_1_0_, person_one0_.name as name2_1_0_ from Anu_PersonOnetoOne person_one0_ where person_one0_.personId=?
NAME IS NIKITA

**NO ADDRESS IS SELECTED**

---------------------- EAGER
**LEFT OUTER JOIN 
Hibernate: 
    select
        person_one0_.personId as personId1_1_0_,
        person_one0_.address_id as address_id3_1_0_,
        person_one0_.name as name2_1_0_,
        address_on1_.id as id1_0_1_,
        address_on1_.city as city2_0_1_,
        address_on1_.pincode as pincode3_0_1_ 
    from
        Anu_PersonOnetoOne person_one0_ 
    left outer join
        Anu_AddressOnetoOne address_on1_ 
            on person_one0_.address_id=address_on1_.id 
    where
        person_one0_.personId=?
*/