package com.beans;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.validator.constraints.Length;
@Entity(name="anu_person_tab")
public class Person {

		//Hibernate Annotation
		
		@Length(min=5,message="Enter within 5 charactes")//Validation put on name varaible
		
		private String name;
		@Id
		@GeneratedValue(strategy=GenerationType.AUTO)
		private int personId;
		private Date birthDate;
		
		@Override
		public String toString() {
			return "Person [name=" + name + ", personId=" + personId + ", birthDate=" + birthDate + "]";
		}
		
		public String getName() {
			return name;
		}
		
		public void setName(String name) {
			this.name = name;
		}
		
		public int getPersonId() {
			return personId;
		}
		
		public void setPersonId(int personId) {
			this.personId = personId;
		}
		
		public Date getBirthDate() {
			return birthDate;
		
		}
		
		public void setBirthDate(Date birthDate) {
		
			this.birthDate = birthDate;
		
		}
		
		public Person(String name, int personId, Date birthDate) {
		
			super();
			this.name = name;
			this.personId = personId;
			this.birthDate = birthDate;
		
		}


		public Person() {
		
		// TODO Auto-generated constructor stub
		
		}

}