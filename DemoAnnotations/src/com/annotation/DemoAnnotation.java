package com.annotation;

@MyDataAnnotaion(name="class",id=896,age=78)     //for class
public class DemoAnnotation {
	
	
@MyAnnotation(age=10,name="anuja",newNames= {"a","b"},value="myvalue") //for method
@MyDataAnnotaion(name="anuja",id=123)
public void display()
{
	System.out.println("hello");
}

@MyAnnotation(age=10,name="anuja_watpade",newNames= {"a","b"},value="myvalue") //for method
@MyDataAnnotaion(name="anuja",id=123)
public void display(String a)
{
	System.out.println("hello "+a);
}

@MyDataAnnotaion(name="const",id=456,address="nashik")     //for constructor
public DemoAnnotation()
{
	
}
}
