package com.inheritance;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Client_Inheritance {

	public static void main(String[] args) {
		ApplicationContext context=new ClassPathXmlApplicationContext("beans.xml");//load factory and create object
		Parent p=(Parent)context.getBean("parent");
		System.out.println(p);
		Child c1=(Child)context.getBean("child");
		System.out.println(c1);
	}
}
